package Fedora::Rebuild::Repository;
use strict;
use warnings;
use version 0.77; our $VERSION = version->declare("v0.12.1");

use threads;
use threads::shared;
use Moose;
use Carp;
use Encode ();
use File::Path::Tiny;
use File::Spec;
use HTTP::Daemon;
use HTTP::Status qw(:constants :is status_message);
use Fedora::Rebuild::Execute;
use namespace::clean;
use URI::Escape ();

# Directory where the repository exists
has 'path' => ( is => 'ro', isa => 'Str', required => 1);

# Private attributes
has 'pid' => ( is => 'rw', isa => 'Int', lazy=> 1, init_arg => undef,
    default => 0);
has 'url' => ( is => 'rw', isa => 'Maybe[Str]', lazy=> 1, init_arg => undef,
    default => undef);

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;
    my %attrs = @_;

    if (! defined $attrs{'path'} || $attrs{'path'} eq '') {
        croak "Path must be a non-empty string";
    }

    $attrs{'path'} = File::Spec->rel2abs($attrs{'path'});
    if (! -d $attrs{'path'}) {
        File::Path::Tiny::mk($attrs{'path'}) or
            croak("Could not create repository directory `". $attrs{'path'} .
                "': $!\n");
    }

    return $class->$orig(%attrs);
};

# Make object shared between threads to prevent killing daemon by DEMOLISH
# from scheduler threads.
# XXX: No attributes are shared automatically.
around 'new' => sub {
    my $orig = shift;
    my $class = shift;
    return shared_clone($class->$orig(@_));
};

# Kill the server process on object desctruction
sub DEMOLISH {
    shift->stop;
}

# Insert binary RPM files built from the package, the argument, into
# repository. It will not update the YUM metatada. You need to update the
# reporitoty explicitly.
# Croaks on failure.
sub insert {
    my ($self, $package) = @_;
    for my $file ($package->listbinaryrpmfiles) {
        my ($volume, $prefix, $file_name) = File::Spec->splitpath($file);
        my $link = File::Spec->catfile($self->path, $file_name);
        my $target = File::Spec->abs2rel($file, $self->path);
        if (-l $link or -e $link) {
            unlink $link or croak("Could not remove old `" . $link .
                "': $!\n");
        }
        if (!symlink($target, $link)) {
            croak("Could not insert RPM file `" . $file . "' of package `" .
                $package->name . "' into `" . $self->path . "': $!\n");
        }
    }
    return 1;
}

# Update the repository.
# This updates YUM metadata.
sub update {
    my $self = shift;
    
    print "Updating YUM repository of already rebuilt packages...\n";

    # Remove stray symlinks first, createrepo dies on that.
    opendir(my $dir, $self->path) or
        croak("Could not open `" . $self->path . "' directory: $!\n");
    for my $file (readdir($dir)) {
        $file = File::Spec->catfile($self->path, $file);
        if (-l $file and !-e $file) {
            print "Removing stray symlink `", $file, "'\n";
            unlink $file or croak("Could not remove stray symlink `" . $file .
                "': $!\n");
        }
    }
    closedir($dir);

    # Update the repository
    if (!Fedora::Rebuild::Execute->new->do(
            $self->path, 'createrepo_c', '--update', '.')) {
        croak("Could not update repository.\n");
    }
    print "Repository updated successufully.\n";
    return 1;
}


# Start to serve the repository over HTTP.
# Return URL. undef in case of error.
sub start {
    my ($self) = @_;

    if ($self->pid && kill(0, $self->pid)) {
        # Server is already running
        return $self->url;
    }
    
    # Start new server
    $self->pid(0);
    $self->url(undef);
    my $server = HTTP::Daemon->new( 'LocalAddr' => 'localhost' );
    if (!defined($server)) {
        return undef;
    }
    my $daemon = fork;
    if (!defined $daemon) {
        # Fork failed
        undef $server;
        return undef;
    } elsif ($daemon == 0) {
        # Daemon
        $self->run_daemon($server);
        exit 0;
    } else {
        # Parent
        $self->pid($daemon);
        $self->url($server->url);
        return $self->url;
    }
}


# Stop serving the repository
sub stop {
    my $self = shift;
    if ($self->pid) {
        kill 9, $self->pid;
        waitpid($self->pid, 0);
        $self->pid(0);
    }
}


# Private method
sub run_daemon {
    my ($self, $server) = @_;
    if (! defined $server) { return -1; }

    my $connection;
    while (1) {
        if (defined $connection) {
            $connection->close;
            undef($connection);
        }

        $connection = $server->accept;
        my $request = $connection->get_request;
        $connection->force_last_request;

        if (! defined $request) {
            print STDERR "repository server: Bad request\n";
            next;
        }
        if ($request->method eq 'GET') {
            my $rpath = Encode::decode_utf8(
                URI::Escape::uri_unescape($request->uri->path)
            );
            if ($rpath =~ m|\.\./|) {
                $connection->send_error(HTTP_FORBIDDEN,
                    "Parent directories are fobidden");
                next;
            }
            $rpath =~ s|^/||;
            $connection->send_file_response(
                File::Spec->catfile($self->path, $rpath));
        } else {
            $connection->send_error(HTTP_NOT_IMPLEMENTED,
                "Only GET method is supported");
        }
    }    
}

1;
