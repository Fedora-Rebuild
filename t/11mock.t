#!/usr/bin/perl
use strict;
use warnings;
use Test::Simple tests => (3 * 4 + 0 + 1 + 2) + (3) + (3);

use Fedora::Rebuild::Mock;
use File::Path::Tiny;
use File::Spec;
use URI;


# Each call does 1 test.
sub expect_croak {
    my ($arch, $repos) = @_;

    my $mock;
    eval {
        no warnings 'all';
        $mock = Fedora::Rebuild::Mock->new(
            architecture => $arch,
            repositories => $repos
        );
    };

    ok ( $@ ne '', "architecure <$arch> is forbidden");
    unlink $mock->config_file if defined $mock;
}


# Each call does 4 + <number_of_repositories> tests.
sub do_test {
    my ($arch, $repos) = @_;

    my $mock = Fedora::Rebuild::Mock->new(
        architecture => $arch,
        repositories => $repos
    );
    my ($directory, $root) = $mock->make_configuration;

    ok(defined $directory, 'Method make_configuration returns the directory');
    ok(defined $root, 'Method make_configuration returns the root name');

    my $filename = File::Spec->catfile($directory, ($root . '.cfg'));
    open(my $fh, '<', $filename);
    my $content = '';
    while(<$fh>) {
        $content .= $_;
        if (/config_opts\['target_arch'\]\s*=\s*'([^']*)'/) {
            ok ( ($1 eq $arch),
                "target architecture: expected=<$arch> got=<$1>")
        }
        if (/config_opts\['legal_host_arches'\]\s*=\s*\('([^']*)',\)/) {
            ok ( ($1 eq $arch),
                "host architectures: expected=<$arch> got=<$1>")
        }
    }
    close($fh);

    for (@$repos) {
        my $repo = URI->new($_);
        ok ( $content =~ /[^#]*baseurl\s*=\s*\Q$repo\E/,
            "$repo is referrenced");
    }

    File::Path::Tiny::rm($directory);
}


do_test('a', [ ]);
do_test('a', [ 'http://example.com/' ]);
do_test('a', [ 'http://1.example.com/', 'http://2.example.com/' ]);

expect_croak(q{'}, []);
expect_croak(q{"}, []);
expect_croak(qq{\n}, []);

# Shared mock caches
{
    my ($arch, $repos) = ('q', [ 'http://example.com/'] );

    my ($directory1, $root1) = Fedora::Rebuild::Mock->new(
        architecture => $arch,
        repositories => $repos,
        shared => 1
    )->make_configuration;

    my $filename = File::Spec->catfile($directory1, ($root1 . '.cfg'));
    open(my $fh, '<', $filename);
    my $content1 = '';
    while(<$fh>) { $content1 .= $_; }
    close($fh);

    for my $identifier (qw(ccache_enable yum_cache_enable root_cache_enable)) { 
        ok ($content1 =~ /\b\Q$identifier\E'\s*\]\s*=\s*True\b/,
            "shared mock has enabled $identifier");
    }

    File::Path::Tiny::rm($directory1);
}
