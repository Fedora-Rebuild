package Fedora::Rebuild;
use strict;
use warnings;
use version 0.77; our $VERSION = version->declare("v0.12.1");

use Moose;
use Moose::Util::TypeConstraints;
use File::Path::Tiny;
use Carp;
use IO::Handle;
use Fedora::Rebuild::Types qw( Mode );
use Fedora::Rebuild::Execute;
use Fedora::Rebuild::Mock;
use Fedora::Rebuild::Package;
use Fedora::Rebuild::Package::Config;
use Fedora::Rebuild::Repository;
use Fedora::Rebuild::Scheduler;
use Fedora::Rebuild::Set::Package;
use Fedora::Rebuild::Solver;
use namespace::clean;

has 'all' => (is => 'ro', isa => 'Str', required => 1);
has 'done' => (is => 'ro', isa => 'Str', required => 1);
has 'failed' => (is => 'ro', isa => 'Str', required => 1);
# A file to log build order. Default is 'log'.
has 'log'   => (is => 'ro', isa => 'Str', required => 0, default => 'log');
has 'workdir' => (is => 'ro', isa => 'Str', required => 1);
# Directory where repository with built packages live. This is needed for
# `mock' build mode.
has 'repodir' => (is => 'ro', isa => 'Str', required => 1);
# Reference to array of repository URLs. Usually points to Fedora mirror or
# Koji repository (baseurl in YUM terminology). This is needed for `mock'
# build mode.
has 'repourls' => (is => 'rw', isa => 'ArrayRef[Str]', required => 1);
# YUM packages to install at mock chroot initialization. Separate them by
# a space.
# '@buildsys-build' is needed for mirrored Fedora repositories
# '@build' is needed for direct koji repositories.
# Default value is '@buildsys-build'.
has mock_install_packages => ( is => 'ro', isa => 'Str', required => 0,
    default => '@buildsys-build');
# RPM architecture name, e.g. x86_64.
# TODO: Get current architecture by rpmGetArchInfo() from librpm
has 'architecture' => (is => 'ro', isa => 'Str', required => 1);
# pyrpkg front-end for handling dist-git repositories. Default is `fedpkg'.
has 'pyrpkg' => (is => 'ro', isa => 'Str', required => 0, default => 'fedpkg');
# Koji client command. Default is `koji'.
has 'koji' => (is => 'ro', isa => 'Str', required => 0, default => 'koji');
# Clone git repositories anonymously. This will not work with `koji' mode.
# Default is non-anonymous clone which requires preconfigured SSH public key
# authentication.
has 'anonymous' => (is => 'ro', isa => 'Bool', required => 0, default => 0);
# Git branch name
# "f14", "f15" etc. Use "rawhide" for latest one.
has 'dist' => (is => 'ro', isa => 'Str', required => 1);
# Fedpkg dist name identifier. This is required only if git branch name is not
# an offical one (e.g. a private branch).
# "f14", "f15" etc. Use "f18" for the latest one.
has 'pkgdist' => (is => 'ro', isa => 'Maybe[Str]', required => 0,
    default => undef);
# Build target name
# "dist-f14", "dist-f15" etc. Use "dist-rawhide" for latest one.
has 'target' => (is => 'ro', isa => 'Str', required => 1);
has 'message' => (is => 'ro', isa => 'Str', required => 1);
# Reference to function with three arguments (BuildRequire name, relation
# flag, version) returning false the BuildRequire should be considered, true
# otherwise. If attribute is undef or missing, no filtering will be performed
# (i.e. the same effect as sub {1}).
has 'buildrequiresfilter' => (is => 'ro', isa => 'CodeRef', required => 0);
# Pass "local" committing and building locally only, pass "mock" to committing
# localy and building in mock, pass "koji" for pushing commits and building
# in Koji. Default is "koji" to build in Koji.
has 'mode' => (is => 'ro', isa => Mode, required => 0, default => 'koji');
# Run rebuild in given number of threads. Default is 1.
has 'threads' => (is => 'ro', isa => subtype('Int' => where {$_>0} =>
        message {"Attribute threads must be positive integer (was $_)"}),
    required => 0);
# Load binary provides of already done packages or buildrequires in given
# number of threads. This is to lower local I/O load. Default is 1. 
has 'loadthreads' => (is => 'ro', isa => subtype('Int' => where {$_>0} =>
        message {"Attribute loadthreads must be positive integer (was $_)"}),
    required => 0);
# Select rebuildable packages in given number of threads.
# Dependency solver is CPU intentsive work.
# Default is 1. 
has 'selectthreads' => (is => 'ro', isa => subtype('Int' => where {$_>0} =>
        message {"Attribute selectthreads must be positive integer (was $_)"}),
    required => 0);
# Maximal count of immediately sequential failures to accept and continue
# rebuild process. If the limit exceeds, rebuild process will terminate. This
# is good to catch pathologic cases when something is obviously wrong and
# should be fixed before rebuilding (e.g. Koji is down, or you have not SSH
# key loaded into SSH agent). Use non-positive number to disable this check.
# Default value is 0 (i.e. not to check).
has 'failurelimit' => (is => 'ro', isa => 'Int', required => 0, default => 0);
# Build packages in dependency order. Default is true.
# If set to false, the packages will be built irrespective to dependecies
# (build- and run-time).
has 'ordered' => (is => 'ro', isa => 'Bool', required => 0, default => 1);
# Trace dependency solver verdict. Default is true.
# If set to false, reason why a package is (not) buildable will not be
# available. Contratry, it save a memory.
has 'verbosesolver' => (is => 'ro', isa => 'Bool', required => 0, default => 1);

has 'remaining_packages' => (is => 'ro', isa => 'Fedora::Rebuild::Set::Package',
    lazy_build => 1, init_arg => undef);
has 'done_packages' => (is => 'ro', isa => 'Fedora::Rebuild::Set::Package',
    lazy_build => 1, init_arg => undef);
has 'failed_packages' => (is => 'ro', isa => 'Fedora::Rebuild::Set::Package',
    lazy_build => 1, init_arg => undef);
has 'subsequent_failures' => (is => 'rw', isa => 'Int', default => 0,
    init_arg => undef);
has 'last_failed' => (is => 'rw', isa => 'Bool', default => 0,
    init_arg => undef);
has 'repository' => (is => 'ro', isa => 'Fedora::Rebuild::Repository',
    lazy_build => 1, init_arg => undef);
has build_config => (is => 'ro', isa => 'Fedora::Rebuild::Package::Config',
    lazy_build => 1, init_arg => undef);


# Creates set of packages already rebuilt.
sub _build_done_packages {
    my $self = shift;
    my $packages = Fedora::Rebuild::Set::Package->new();
    my $file;

    if (not -e $self->done) {
        print "No packages have been rebuilt yet.\n";
        return $packages;
    }

    open($file, '<', $self->done) or
        croak "Could not open `" . $self->done .
            "' for loading list of already rebuilt packages: $!";
    print "Loading list of already rebuilt package names...\n";
    while (local $_ = <$file>) {
        chomp;
        if (m/^\s*$/) { next; }

        my $package = Fedora::Rebuild::Package->new(name => $_,
            workdir => $self->workdir, dist => $self->dist,
            pkgdist => $self->pkgdist, target => $self->target,
            message => $self->message);
        $packages->insert($package);
        $self->repository->insert($package);
    }
    if ($!) {
        croak "Could not read list of rebuilt package names from file `" .
        $self->done . "': $!";
    }
    close $file;
    $self->repository->update;
    print "Number of done packages: " . $packages->size() . "\n";
   
    return $packages;
}


# Set counter of consequently failures to zero.
sub reset_failure_counter {
    my $self = shift;
    $self->last_failed(0);
    $self->subsequent_failures(0);
}

# Append a line to a file. It adds a new line character.
sub append_to_file {
    my ($self, $file_name, $text) = @_;

    my $file = IO::Handle->new();
    open($file, '>>', $file_name) or
        croak "Could not open `" . $file_name . "' for appending: $!";
    printf $file $text . "\n" or
        croak "Could not write data into `" . $file_name . "': $!";
    $file->flush && $file->sync && close($file) or
        croak "Could not flush and close `" . $file_name . "': $!";
}

sub mark_done {
    my ($self, $package) = @_;
    
    $self->append_to_file($self->done, $package->name);
    $self->append_to_file($self->log, $package->name);

    $self->done_packages->insert($package);
    $self->reset_failure_counter;
}

# Creates set of packages not yet rebuilt not already failed.
sub _build_remaining_packages {
    my $self = shift;
    my $packages = Fedora::Rebuild::Set::Package->new();
    my $file;

    open($file, '<', $self->all) or
        croak "Could not open " . $self->all .
            " for loading list of package names to rebuild: $!";
    print "Loading list of all package names to rebuild...\n";

    while (local $_ = <$file>) {
        chomp;
        if (m/^\s*$/) { next; }
        if ($packages->contains($_)) { next; }
        if (! $self->done_packages->contains($_) &&
                ! $self->failed_packages->contains($_)) {
            $packages->insert(Fedora::Rebuild::Package->new(name => $_,
                workdir => $self->workdir, dist => $self->dist,
                pkgdist => $self->pkgdist, target => $self->target,
                message => $self->message));
        }
    }
    if ($!) {
        croak "Could not read list of package names to rebuild from file `" .
            $self->all . "' :$!";
    }
    close $file;

    print "Number of all packages: " . 
        ($packages->size + $self->done_packages->size
            + $self->failed_packages->size) . "\n";
    print "Number of not yet rebuilt packages: " . $packages->size() . "\n";
    return $packages;
}


# Set of packages whose rebuild failed.
sub _build_failed_packages {
    my $self = shift;
    my $packages = Fedora::Rebuild::Set::Package->new();
    my $file;

    if (not -e $self->failed) {
        print "No packages have failed yet.\n";
        return $packages;
    }

    open($file, '<', $self->failed) or
        croak "Could not open `" . $self->failed .
            "' for loading list of already failed packages: $!";
    print "Loading list of already failed package names...\n";
    while (local $_ = <$file>) {
        chomp;
        if (m/^\s*$/) { next; }

        my $package = Fedora::Rebuild::Package->new(name => $_,
            workdir => $self->workdir, dist => $self->dist,
            pkgdist => $self->pkgdist, target => $self->target,
            message => $self->message);
        $packages->insert($package);
    }
    if ($!) {
        croak "Could not read list of failed package names from file `" .
        $self->failed . "': $!";
    }
    close $file;
    print "Number of failed packages: " . $packages->size() . "\n";
    
    return $packages;
}

sub _build_repository {
    my $self = shift;
    return Fedora::Rebuild::Repository->new(path => $self->repodir);
}

# Gather build configuration into one object to ease passing it to
# Fedora::Rebuild::Package methods.
sub _build_build_config {
    my $self = shift;
    return Fedora::Rebuild::Package::Config->new(
        mode => $self->mode(),
        pyrpkg => $self->pyrpkg(),
        koji => $self->koji(),
        repositories => $self->repourls(),
        architecture => $self->architecture(),
        mock_install_packages => $self->mock_install_packages()
    );
}

# Record package names into log of failed packages
sub mark_failed {
    my ($self, $package) = @_;
    
    # Log failure
    $self->append_to_file($self->failed, $package->name);

    # Move to list of failed
    $self->failed_packages->insert($package);

    # Check for very failures
    if ($self->last_failed) {
        $self->subsequent_failures($self->subsequent_failures + 1);
    } else {
        $self->last_failed(1);
        $self->subsequent_failures(1);
    }
    if ($self->failurelimit > 0 &&
            $self->subsequent_failures > $self->failurelimit) {
        croak "More then " . $self->failurelimit .
            " package(s) failed subsequently which is more then set " .
            "threshold. Aborting now.\n";
    }
}

# Load build-requires for each not-yet done package.
# Arguments are initialized mocks used build source packages.
# Return true in case of success or croaks in case of failure.
sub load_sourcedependencies {
    my ($self, @mocks) = @_;
    my @packages = $self->remaining_packages->packages;
    my $scheduler = Fedora::Rebuild::Scheduler->new(
        limit => $self->loadthreads,
        name => 'Loading build-requires',
        total => $#packages
    );
    my %jobs = ();
    my $i = 0;
    my @available_mocks = @mocks;
    
    print "Loading build-time dependenices of not yet rebuilt packages...\n";

    foreach my $package (@packages) {
        my $mock = pop @available_mocks;
        my $job = $scheduler->schedule($package->can('get_buildrequires'),
            $package, $self->build_config, $self->anonymous, $mock);
        if (! defined $job) {
            push @available_mocks, $mock;
            next;
        }
        $jobs{$job} = [ $package, $mock ];
        my %finished = $scheduler->finish(++$i < @packages);

        while (my ($job, $status) = each %finished) {
            my ($package, $mock) = @{$jobs{$job}};
            push @available_mocks, $mock;
            if (!$$status[0]) {
                print "Could not load build-time dependencies for not yet " .
                    "built package `" . $package->name . "'.\n";
                print "Waiting for finishing scheduled jobs...\n";
                $scheduler->finish(1);
                print "All jobs loading build-time dependcies have finished.\n";
                $self->clean_mocks(@mocks);
                croak "Could not load build-time dependencies.\n";
            }
        }
    }

    print "Build-time dependencies of not-yet rebuilt packages loaded " .
        "successfully.\n";
    return 1;
}


# Load binary requires and provides of each done package.
# Return true in case of success or croaks in case of failure.
sub load_binarydependencies {
    my $self = shift;
    my @packages = $self->done_packages->packages;
    my $scheduler = Fedora::Rebuild::Scheduler->new(
        limit => $self->loadthreads,
        name => 'Loading binary dependendies',
        total => $#packages
    );
    my %jobs = ();
    my $i = 0;
    
    print "Loading binary dependenices of already rebuilt packages...\n";

    foreach my $package (@packages) {
        my $job = $scheduler->schedule($package->can('get_binarydependencies'),
            $package);
        if (! defined $job) { next; }
        $jobs{$job} = $package;
        my %finished = $scheduler->finish(++$i < @packages);

        while (my ($job, $status) = each %finished) {
            my $package = $jobs{$job};
            if (!$$status[0]) {
                print "Could not load binary dependencies for already built " .
                    "package `" . $package->name . "'.\n";
                print "Waiting for finishing scheduled jobs...\n";
                $scheduler->finish(1);
                print "All jobs loading binary dependcies have finished.\n";
                croak "Could not load binary dependencies\n";
            }
        }
    }

    print "Binary dependencies of already rebuilt packages loaded " .
        "successfully.\n";
    return 1;
}


# Decides a package is rebuildable now.
# Return 0 for false, 1 or true, undef for error while deciding.
sub is_rebuildable {
    my ($self, $solver, $package) = @_;
    my $is_rebuildable;
    my $message = '';

    if ($self->ordered) { 
        $is_rebuildable = $solver->is_buildable($package, 
            ($self->verbosesolver) ? \$message : undef);
        if (!$self->verbosesolver) {
            $message = 'Traceing the reason has been suppressed'
        }
    } else {
        $is_rebuildable = 1;
        $message = "Package `" . $package->name .
            "' is buildable because unordered build mode is selected.";
    }

    $package->log_is_rebuildable($is_rebuildable, $message);
    if ($self->verbosesolver) {
        print "$message\n";
    } else {
        if ($is_rebuildable) {
            print "Source package `" . $package->name .
                "' can be rebuilt now.\n";
        } else {
            print "Source package `" . $package->name .
                "' cannot be rebuilt now.\n";
        }
    }
    return $is_rebuildable;
}


# Return F:R:Set:Packages than can be rebuilt now
sub select_rebuildable {
    my $self = shift;
    my @packages = $self->remaining_packages->packages;
    my $scheduler = Fedora::Rebuild::Scheduler->new(
        limit => $self->selectthreads,
        name => 'Selecting buildable packages',
        total => $#packages
    );
    my %jobs = ();
    my $i = 0;
    print "Selecting rebuildable packages...\n";

    my $solver;
    if ($self->ordered) {
        print "Building providers reverse cache...\n";
        $solver = Fedora::Rebuild::Solver->new(
            'packages' => $self->done_packages,
            'dependencyfilter' => $self->buildrequiresfilter);
        print "Providers reverse cache built.\n";
    }
    my $rebuildables = Fedora::Rebuild::Set::Package->new;
    foreach my $package (@packages) {
        my $job = $scheduler->schedule($self->can('is_rebuildable'), $self,
            $solver, $package);
        if (! defined $job) { next; }
        $jobs{$job} = $package;
        my %finished = $scheduler->finish(++$i < @packages);

        while (my ($job, $status) = each %finished) {
            my $package = $jobs{$job};

            if ($$status[0]) {
                $rebuildables->insert($package);
            } elsif (!defined $$status[0]) {
                # Could not decide whether the $package is rebuildable. This is
                # fatal for the package. Move it to failed packages.
                $self->remaining_packages->delete($package);
                $self->mark_failed($package);
            }
        }
    }

    print "Packages selected to rebuild (" . $rebuildables->size .
        "): " . $rebuildables->string . "\n";
    return $rebuildables;
}


# This removes trees of each F:R:Mock object passed as an argument.
sub clean_mocks {
    my ($self, @mocks) = @_;
    print "Removing mock environments...\n";
    my $success = 1;
    for my $mock (@mocks) {
        if (!Fedora::Rebuild::Execute->new->do(undef, 'mock',
                    '--configdir', $mock->config_dir,
                    '--root', $mock->config_root,
                    '--scrub=all')) {
            print "Error while removing `", $mock->config_dir,
                "' mock environment\n";
            $success = 0;
        }
        File::Path::Tiny::rm($mock->config_dir);
    }
    print "Removing done.\n";
    return $success;
}


# Return array of F:R:Mock objects. The mocks will be shared and possibly
# initialized. Caller is responsible for cleaning them.
# First argument is number of mocks to prepeare.
# Second argument is true to initialize the mocks and keep them so.
# This croaks on error. It returns list of the Mock objects.
sub prepare_mocks {
    my ($self, $count, $initialize) = @_;

    my @mocks;

    foreach my $i (1 .. $count) {
        print "Preparing shared mock environment $i of $count...\n";

        my $mock;
        eval {
            $mock = Fedora::Rebuild::Mock->new(
                    architecture => $self->architecture,
                    repositories => $self->repourls,
                    shared => 1,
                    install_packages => $self->mock_install_packages,
            );
            $mock->make_configuration;
        };

        if ($@) {
            $self->clean_mocks(@mocks);
            croak("Could not configure mock: $@\n");
        }

        if ($initialize) {
            if (!Fedora::Rebuild::Execute->new->do(undef, 'mock',
                    '--configdir', $mock->config_dir,
                    '--root', $mock->config_root,
                    '--no-cleanup-after',
                    '--init')) {
                $self->clean_mocks(@mocks);
                croak("Could not initilize shared mock environment.\n");
            }
        }

        push @mocks, $mock;
    }

    print "Shared mock environments prepared successfully.\n";

    return @mocks;
}


# Rebuild all remaining packages
sub run {
    my $self = shift;
    if (defined $self->repository) {
        if (! defined $self->repository->url) {
            print "Starting repository HTTP server...\n";
            $self->repository->update;
            $self->repository->start;
            push @{$self->repourls}, $self->repository->url;
        }
        print "Repository URL is: <" . $self->repository->url . ">\n";
    }

    {
        print STDERR "Applying threaded glob() crash work-around\n";
        local $_ = glob();
    }

    print "remaining_packages: " . $self->remaining_packages->string . "\n";
    print "done_packages: " . $self->done_packages->string . "\n";
    print "Rebuild mode: " . $self->mode .
        " " . (($self->ordered)?"ordered":"unordered") . "\n";

    my @src_mocks;
    if ($self->mode eq 'koji' or $self->mode eq 'mock') {
        # XXX: we need one more mock because a mock is selected before
        # the scheduler blocks.
        my $count = $self->loadthreads;
        if ($self->mode eq 'mock' and $self->threads > $count) {
            $count = $self->threads;
        }
        @src_mocks = $self->prepare_mocks(1 + $count, 1);
    }

    if ($self->ordered) {
        $self->load_sourcedependencies(@src_mocks);
        $self->load_binarydependencies;
    }

    my @mocks;
    if ($self->mode eq 'mock') {
        # xxx: we need one more mock because a mock is selected before
        # the scheduler blocks.
        @mocks = $self->prepare_mocks(1 + $self->threads, 0);
    }

    while ($self->remaining_packages->size > 0) {
        my $rebuildable_packages = $self->select_rebuildable;
        if ($rebuildable_packages->size == 0) {
            printf "No more packages can be rebuilt!\n";
            last;
        }

        my $scheduler = Fedora::Rebuild::Scheduler->new(
            limit => $self->threads,
            name => 'Building',
            total => $rebuildable_packages->size
        );

        # Rebuild packages
        my %jobs = ();
        my @packages = $rebuildable_packages->packages;
        my $i = 0;
        my @available_mocks = @mocks;
        my @available_src_mocks = @src_mocks;

        foreach my $package (@packages) {
            $self->remaining_packages->delete($package);

            my $mock = pop @available_mocks;
            my $src_mock = pop @available_src_mocks;
            my $job = $scheduler->schedule($package->can('rebuild'), $package,
                $self->build_config, $self->anonymous, $mock, $src_mock);
            if (! defined $job) {
                push @available_mocks, $mock;
                push @available_src_mocks, $src_mock;
                next;
            }
            $jobs{$job} = [ $package, $mock, $src_mock ];
            my %finished = $scheduler->finish(++$i < @packages);
            #print STDERR "HIT A\n";        
            #local $_ = glob();
            #print STDERR "HIT B\n";        

            while (my ($job, $status) = each %finished) {
                my ($package, $mock, $src_mock) = @{$jobs{$job}};
                push @available_mocks, $mock;
                push @available_src_mocks, $src_mock;
                if ($$status[0]) {
                    # TODO: Push Provides into global list of available
                    # provides.
                    # XXX: Nothing here, fall through to rotation waiting,
                    # rebuilt package list tracked in $rebuildable_packages.
                    $self->reset_failure_counter;
                } else {
                    $rebuildable_packages->delete($package);
                    $self->mark_failed($package);
                }
            }
        }


        # Wait for Koji rotation
        # This is separeted from rebuilding process to solve trade-off between
        # high threads number (consumes lot of memory and CPU time when
        # starting) and the rebuild itself is much faster Koji rotation period
        # (lot of time lost to wait for rotation).
        %jobs = ();
        @packages = $rebuildable_packages->packages;
        $i = 0;
        
        $scheduler = Fedora::Rebuild::Scheduler->new(
            limit => $self->threads,
            name => 'Waiting for build root',
            total => $rebuildable_packages->size
        );

        foreach my $package (@packages) {
            my $job = $scheduler->schedule($package->can('waitforbuildroot'),
                $package, $self->build_config);
            if (! defined $job) { next; }
            $jobs{$job} = $package;
            my %finished = $scheduler->finish(++$i < @packages);

            while (my ($job, $status) = each %finished) {
                my $package = $jobs{$job};
                if ($$status[0]) {
                    $self->repository->insert($package);
                    $self->mark_done($package);
                } else {
                    $self->mark_failed($package);
                }
            }
        }
        $self->repository->update;
        $self->append_to_file($self->log, '# Synchronization point reached');
    }

    if ($self->mode eq 'koji' or $self->mode eq 'mock') {
        $self->clean_mocks(@src_mocks);
    }
    if ($self->mode eq 'mock') {
        $self->clean_mocks(@mocks);
    }

    if ($self->remaining_packages->size > 0) {
        print "Following packages have unsatisfied build-time dependencies (" .
        $self->remaining_packages->size . "): " .
        $self->remaining_packages->string . "\n";
    }
    if ($self->failed_packages->size > 0) {
        print "Following packages have failed while rebuilding (" .
            $self->failed_packages->size . "): " .
            $self->failed_packages->string . "\n";
        return 0;
    } else {
        print "All rebuildable packages have been rebuilt.\n";
        return 1;
    }
}

1;
__END__
=encoding utf8

=head1 NAME

Fedora::Rebuild - Rebuilds Fedora packages from scratch

=head1 DESCRIPTION

Main goal is to rebuild perl modules packages for Fedora. The rebuild is
driven from bottom to top, i.e. from perl interpreter to modules depending on
intermediate modules. This way, it's possible to upgrade perl interpreter to
incompatible version and to rebuild all modules against the new interpreter.

Procedure is following: We have a set of source package to rebuild. We
distill all build-time dependencies for each source package. We ignore
non-perl dependencies as we focus on Perl only.

We select all source packages that do not depend on anything and push them
into Koji to rebuild. Obviously, the only fulfilling package is Perl interpret
only.

Once the first subset of packages is rebuilt, we gather their binary packages
and distill their binary provides. These freshly available provides are put
into new set of available provides. At the same time we remove the rebuilt
subset from original set of all source packages.

Then we wait for build root rotation in Koji to get rebuilt binary packages
available in build root.

(Of course we could get provides of new binary packages from koji repository
after rotation and we can get provides through repoquery instead of
inspecting binary packages manually.)

If package rebuild fails, the package is removed from future consideration.

Then we return to start of this procedure to select other subset of source
packages whose build-time dependecies can be satisfied from set of binary
provides we have obtained till now.

This loop cycles until set of source packages is empty.

=head1 SYNCHRONIZATION

Because mass rebuild is long term issue and lot of intermediate failures can
emerge, one must be able to resume failed rebuild process.

Safe resume is assured by proper logging. Once a package has been rebuilt, its
name is logged into list of done packages. Restarting rebuild program with the
same arguments loads done packages, remove them from set of remaining
packages, and populates list of available provides. In additon, packages whose
rebuild failed are removed from set of remaining packages.

=head1 AUTHOR

Petr Písař <ppisar@redhat.com>

=head1 COPYING

Copyright (C) 2011, 2012, 2013, 2014  Petr Pisar <ppisar@redhat.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

