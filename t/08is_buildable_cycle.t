#!/usr/bin/perl
use strict;
use warnings;

use Test::Simple tests => 6;

use Fedora::Rebuild::Solver;
use Fedora::Rebuild::Set::Package;
use Fedora::Rebuild::Package;
use Fedora::Rebuild::RPM;
use threads::shared;

# Prepare packages
# Create and return package with one binary package that always provides its
# unversioned name.
sub build_package {
    my ($name, $provides, $runrequires, $buildrequires) = @_;

    my $package = Fedora::Rebuild::Package->new(
        'name' => $name,
        workdir => 't/tmp',
        dist => 'foo',
        target => 'foo',
        message => 'foo',
    );

    $package->provides(shared_clone(
            {
                $name => {
                    $name => [[Fedora::Rebuild::RPM::ANY, '']],
                    %$provides
                }
            }
        ));
    $package->runrequires(shared_clone(
            { $name => $runrequires }
        ));
    $package->requires(shared_clone($buildrequires));
    return $package;
}

my $A = build_package(
    'A',
    {},
    { 'A' => [[Fedora::Rebuild::RPM::ANY, '']] },
    { 'A' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

my $B = build_package(
    'B',
    {},
    {},
    { 'A' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

my $C = build_package(
    'C',
    {},
    { 'D' => [[Fedora::Rebuild::RPM::ANY, '']] },
    { 'D' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

my $D = build_package(
    'D',
    {},
    { 'C' => [[Fedora::Rebuild::RPM::ANY, '']] },
    { 'C' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

my $E = build_package(
    'E',
    {},
    {},
    { 'D' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

my $F = build_package(
    'F',
    {},
    {},
    { 'C' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

# Prepare set of packages
my $packages = Fedora::Rebuild::Set::Package->new();
$packages->insert($A);
$packages->insert($B);
$packages->insert($C);
$packages->insert($D);
$packages->insert($E);
$packages->insert($F);

# Prepare solver
my $solver = Fedora::Rebuild::Solver->new(packages => $packages,
    dependencyfilter => sub {0});

# Test
my $message;

# Cycle with 1 edge
ok($solver->is_buildable($A, \$message),
    "A build-requires itself: $message");
ok($solver->is_buildable($B, \$message),
    "B build-requires reflexive A: $message");

# Cycle with 2 edges
ok($solver->is_buildable($D, \$message),
    "D build-requires C that requires D that requires C: $message");
ok($solver->is_buildable($C, \$message),
    "C build-requires D that requires C that requires D: $message");
ok($solver->is_buildable($E, \$message),
    "E build-requires D that requires C that requires D: $message");
ok($solver->is_buildable($F, \$message),
    "F build-requires C that requires D that requires C: $message");
