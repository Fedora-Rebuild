package Fedora::Rebuild::Solver;
use strict;
use warnings;
use version 0.77; our $VERSION = version->declare("v0.12.1");

use Moose;
use Fedora::Rebuild::Set::Package;
use Fedora::Rebuild::Package;
use Fedora::Rebuild::Providers;
use namespace::clean;

=encoding utf8

=head1 NAME

Fedora::Rebuild::Solver - Dependency solver

=head1 CONSTRUCTOR

=head2 new(packages => $packages, dependencyfilter => sub {})

The constructor I<new> prepares initilialize a depencency solver on top of
a packackage set. Possible attributes are:

=over

=item packages

Set of packages, L<Fedora::Rebuild::Set::Package> object, with populated
provides and requires. This attribute is mandatory.

=cut

has 'packages' => (is => 'ro', isa => 'Fedora::Rebuild::Set::Package',
    lazy_build => 1, required => 1);

=item dependencyfilter

Reference to a function with three arguments (dependency name, relation
flag, version) returning false if the dependency should be considered, true
otherwise. If attribute is C<undef> or missing, no filtering will be performed
(i.e. the same effect as C<sub {1}>). This attribute is optional.

=cut

has 'dependencyfilter' => (is => 'ro', isa => 'CodeRef', required => 0);

=back
=cut

=head1 PRIVATE ATTRIBUTES

=head2 cache

This is cache of binary packages already resolved as installable or not
installable.

To keep coherency, do not update C<< $self->packages >> or referenced packages.
This is a reference to hash of binary package names. Values hold a I<verdict>
(true for installable, false otherwise) and a I<message> (string of
dependencies or reason why not installable).

=cut

has 'cache' => (is => 'rw', isa => 'HashRef', lazy_build => 1,
    init_arg => undef);

=head2 providers

This is a reverse cache of binary packages and their providers.

To keep coherency, do not update C<< $self->packages >> or referenced packages.
This is a L<Fedora::Rebuild::Providers> object. This attribute is built
in constructor to have it ready before spawning threads.

=cut

has 'providers' => (is => 'ro', isa => 'Fedora::Rebuild::Providers',
    lazy_build => 0, init_arg => undef, writer => '_set_providers');

=head1 METHODS
=cut

sub BUILD {
    my $self = shift;
    $self->_set_providers($self->_build_providers);
}

sub _build_cache {
    # TODO: Thread safety?
    return {};
}

=head2 invalidate_cache()

This call invalidates internal cache of verdicts. No arguments are recognized.

=cut

sub invalidate_cache {
    my $self = shift;
    # TODO: Thread safety?
    $self->cache({});
    #print STDERR "Cache invalidated\n";
}

=head2 add_to_cache($binarypackage, $is_installable, $message)

Add C<is_installable()> result into verdict cache.

=cut

sub add_to_cache {
    my ($self, $binarypackage, $is_installable, $message) = @_;

    # TODO: Thread safety?
    ${$self->cache}{$binarypackage} = {
        is_installable => $is_installable,
        message => $message
    };

    #print STDERR "Cache write: $binarypackage, " .
    #    "is_installable=$is_installable, message=$message\n"; 
}


# This is read-only object
sub _build_providers {
    my $self = shift;
    my $providers = Fedora::Rebuild::Providers->new;
    for my $package ($self->packages->packages) {
        $providers->insert($package);
    }
    return $providers;
}

=head2 is_installable_recursive($package, $binarypackage, $message, $verbose, $backpath)

Decides a package is installable now and set string of dependencies.

This is a private method, use L<is_installable()> instead.

=over

=item I<$package>

is a source package object.

=item I<$binarypackage>

is a binary package ENVR as index to $package->runrequies.

=item I<$message>

is reference to variable where to store (not-)found dependencies.
Pass undef if you don't care.

=item I<verbose>

if true, log whole dependency tree. Becuse this can consume a lot of memory
recommended value is false that will cause logging only unsatisfied
dependencies.

=item I<$backpath>

is a reference to list of binary packages on the path in the dependecy tree from
parent to root (i.e. root is last element). It's used to detect a cycle.

=back

Return 0 for false, 1 for true, 2 for cycle. Fill list of dependencies into
I<$$message> (without trailing new line) which is a reference itself.

=cut

sub is_installable_recursive {

    my ($self, $package, $binarypackage, $message, $verbose, $backpath) = @_;

    # Detect cycle
    if (defined $backpath) {
        for my $predecessor (@$backpath) {
            if ($predecessor eq $binarypackage) {
                return 2;
            }
        }
    } else {
        # Although the cache provides correct verdicts (installable vs. not
        # installable), it provides bad messages that were cached for
        # dependency in cycle.
        # Thus invalidate the cache for each top-level package
        $self->invalidate_cache();
    }
    unshift @$backpath, $binarypackage;

    # Check cached result
    if (defined (my $result = ${$self->cache()}{$binarypackage})) {
        if (defined $message) {
            $$message = $$result{message};
            #print STDERR "Cache hit: $binarypackage, message=$$message\n"; 
        }
        shift @$backpath;
        return $$result{is_installable};
    };


    # Each requirement of $binarypackage must be satisfied by at least one
    # binary package from packages list.
    my $binarypackage_runrequires = ${$package->runrequires}{$binarypackage};
    for my $rname (keys %$binarypackage_runrequires) {
        for my $require (@{$$binarypackage_runrequires{$rname}}) {
            my $rflag = $$require[0];
            my $rversion = $$require[1];
            my $is_satisfied = 0;
            my $deep_message;

            if (defined $self->dependencyfilter and
                    &{$self->dependencyfilter}($rname, $rflag, $rversion)) {
                # Avoid user-defined Requires from dependency evaluation
                next;
            }

            ALL_PACKAGES: for my $provider ($self->providers->what_provides(
                    $rname, $rflag, $rversion))
            {
                my ($providing_package, $binary_providing_package) = @$provider;
                    $deep_message = '';
                my $is_installable =
                        $self->is_installable_recursive(
                            $providing_package,
                            $binary_providing_package,
                            (defined $message) ? \$deep_message: undef,
                            $verbose,
                            $backpath);
                if ($is_installable) {
                    $is_satisfied = 1;

                    #print STDERR "$binary_providing_package: " .
                    #    "$is_installable, deep: $deep_message\n";
                    if ($is_installable == 1) {
                        if (defined $message and $verbose) {
                            # Do not log end of cycle
                            if ($$message ne '') {
                                $$message .= ' ';
                            }
                            $$message .= $providing_package->name .
                                 '/' . $binary_providing_package;
                            if ($deep_message ne '')  {
                                $$message .= " ($deep_message)";
                            }
                        }
                    }

                    last ALL_PACKAGES;
                }
            }

            if (!$is_satisfied) {
                if (defined $message) {
                    $$message =  q{`} . $package->name . q{/} . $binarypackage .
                        q{' requires `}. $rname .
                        q{ } .
                        Fedora::Rebuild::RPM::flag_as_string($rflag) .
                        q{ } . $rversion . q{' that cannot be satisfied};
                    if ($deep_message//'' ne '')  {
                        $$message .= " ($deep_message)";
                    }
                }

                # FIXME: Can we cache negative answers for cut-off subtree?
                $self->add_to_cache($binarypackage, 0,
                    (defined $message) ? $$message : undef);
                shift @$backpath;
                return 0;
            }
        }
    }

    # FIXME: We cannot cache package whose descendants were cut
    # off because of breaking cycle.
    # TODO: Cache top-level package only or reevaluate subtree
    # from point of view of the package. (Cyclic problem again?)
    $self->add_to_cache($binarypackage, 1,
        (defined $message) ? $$message : undef);
    shift @$backpath;
    return 1; 
}


=head2 is_installable($self, $package, $binarypackage, $message, $verbose)

Decides a I<$binarypackage> compiled from source I<$package> is installable now.

This is hard problem as it requires recursive evaluation of each package
satisifying run-time dependency of previous package.

Return 0 for false, 1 or true, undef for error while deciding. Fill an error
message (without trailing new line) into reference of third argument
if defined.

=cut

sub is_installable {
    my ($self, $package, $binarypackage, $message, $verbose) = @_;

    my $reason = '';
    if ($self->is_installable_recursive($package, $binarypackage,
            (defined $message) ? \$reason: undef, $verbose, undef)) {
        if (defined $message) {
            $$message = q{RunRequires for `} . $package->name . '/' .
                $binarypackage . q{' fulfilled} .
                (($reason ne '') ? " ($reason)" : '') . '.';
        }
        return 1;
    } else {
        if (defined $message) {
            $$message = q{Package `} . $package->name . '/' . $binarypackage .
                q{' cannot be installed because: } . $reason . '.';
        }
        return 0;
    }
}


=head2 is_buildable($package, $message, $verbose)

Decides a package is buildable now and set string of dependenices.

Return 0 for false, 1 or true. Fill list of dependencies into
I<$message> (without trailing new line) which is a reference itself (or undef
if you don't care).

=cut

sub is_buildable {

    my ($self, $package, $message, $verbose) = @_;

    if (defined $message) {
        $$message = '';
    }

    # Each requirement of $package must be satisfied by at least one binary
    # package from source packages list.
    for my $rname (keys %{$package->requires}) {
        for my $require (@{${$package->requires}{$rname}}) {
            my $rflag = $$require[0];
            my $rversion = $$require[1];
            my $is_satisfied = 0;
            my $deep_message;

            if (defined $self->dependencyfilter and
                    &{$self->dependencyfilter}($rname, $rflag, $rversion)) {
                # Avoid user-defined BuildRequires from dependency evaluation
                next;
            }

            ALL_PACKAGES: for my $provider ($self->providers->what_provides(
                    $rname, $rflag, $rversion))
            {
                my ($providing_package, $binary_providing_package) = @$provider;
                $deep_message = '';
                if ($self->is_installable_recursive($providing_package,
                        $binary_providing_package,
                        (defined $message) ? \$deep_message : undef,
                        $verbose, undef)) {
                    $is_satisfied = 1;
                    if ($message) {
                        if ($$message ne '') {
                            $$message .= ' ';
                        }
                        $$message .= $providing_package->name . '/' .
                            $binary_providing_package;
                        if ($deep_message ne '')  {
                            $$message .= " ($deep_message)";
                        }
                    }
                    last ALL_PACKAGES;
                }
            }

            if (!$is_satisfied) {
                if (defined $message) {
                    $$message =  q{Source package `} . $package->name .
                        q{' cannot be installed because it build-requires `} .
                        $rname .  q{ } .
                        Fedora::Rebuild::RPM::flag_as_string($rflag) .
                        q{ } . $rversion . q{' that cannot be satisfied};
                    if ($deep_message//'' ne '')  {
                        $$message .= " ($deep_message)";
                    }
                    $$message .= '.';
                }
                return 0;
            }
        }
    }

    if (defined $message) {
        $$message = "BuildRequires for `" . $package->name . "' fulfilled" .
            (($$message ne '') ? " ($$message)" : '') . '.';
    }
    return 1; 
}


1;
