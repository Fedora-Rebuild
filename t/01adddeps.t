#!/usr/bin/perl
use strict;
use warnings;
use Test::Simple tests => 5;
use Data::Compare;

use Fedora::Rebuild::RPM;

sub test {
    my ($name, $stash, $needles, $result) = @_;
    Fedora::Rebuild::RPM::adddeps($stash, $needles);
    ok(Compare($stash, $result), $name);
}

test(
    'Disjunct', 
    {
        n1 => [ ['f1', 'v1'] ]
    },
    {
        n2 => [ ['f2', 'v2'] ]
    },
    {
        n1 => [ ['f1', 'v1'] ],
        n2 => [ ['f2', 'v2'] ]
    },
);

test(
    'Identic', 
    {
        n1 => [ ['f1', 'v1'] ]
    },
    {
        n1 => [ ['f1', 'v1'] ]
    },
    {
        n1 => [ ['f1', 'v1'] ]
    }
);

test(
    'Identic name, different flags', 
    {
        n1 => [ ['f1', 'v1'] ]
    },
    {
        n1 => [ ['f2', 'v1'] ]
    },
    {
        n1 => [ ['f1', 'v1'], ['f2', 'v1'] ]
    }
);

test(
    'Identic name, different versions', 
    {
        n1 => [ ['f1', 'v1'] ]
    },
    {
        n1 => [ ['f1', 'v2'] ]
    },
    {
        n1 => [ ['f1', 'v1'], ['f1', 'v2'] ]
    }
);

test(
    'Identic name, some versions differ', 
    {
        n1 => [ ['f1', 'v1'], ['f1', 'v2'], ['f1', 'v3'] ]
    },
    {
        n1 => [ ['f1', 'v1'], ['f1', 'v4'], ['f1', 'v3'] ]
    },
    {
        n1 => [ ['f1', 'v1'], ['f1', 'v2'], ['f1', 'v3'], ['f1', 'v4'] ]
    }
);
