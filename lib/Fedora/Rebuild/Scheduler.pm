package Fedora::Rebuild::Scheduler;
use strict;
use warnings;
use Moose;
use Carp;
use threads;
use threads::shared;
use Thread::Semaphore;
use MooseX::Types::Moose;
use Term::ProgressBar;
use namespace::clean;

use version 0.77; our $VERSION = version->declare("v0.12.1");

# Maximal number of jobs to run in parallel. Default is 1.
has 'limit' => (is => 'ro', isa => 'Int',
    lazy_build => 1, builder => '_build_limit');
# Total number of jobs to proceed. If set to positive number, a progress bar
# will updated and printed to stdout after finishing a job. Default value is
# zero meaning not to print any progress bar.
has 'total' => (is => 'ro', isa => 'Int', lazy_build => 1);
# Label of scheduler to print as title of the progress bar.
has 'name' => (is => 'ro', isa => 'Str', lazy_build => 1);

has 'limiter' => (is => 'ro', isa => 'Thread::Semaphore',
    lazy_build => 1, init_arg => undef);
has 'workers' => (is => 'ro', isa => 'HashRef[Int]', 
    lazy_build => 1, init_arg => undef);
has 'progress' => (is => 'rw', isa => 'Term::ProgressBar',
    lazy_build => 1, init_arg => undef);
has 'done' => (is => 'rw', isa => 'Int',
    lazy_build => 1, init_arg => undef);
has 'next_progress_update' => (is => 'rw', isa => 'Num',
    lazy_build => 1, init_arg => undef);

# Allow to construct as Fedora::Rebuild::Scheduler->new(NUMBER);
around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    if (@_ == 1 && !ref $_[0]) {
        return $class->$orig(limit => $_[0] // 1);
    }
    else {
        return $class->$orig(@_);
    }
};

sub BUILD {
    my $self = shift;

    if ($self->limit < 1) {
        croak "Constructing scheduler with limit bellow 1 dead-locks the " .
            "scheduler";
    }
}

sub _build_limit {
    my ($self, $value) = @_;
    return $value;
}

sub _build_total {
    my ($self, $value) = @_;
    return $value // 0;
}

sub _build_name {
    my ($self, $value) = @_;
    return $value;
}

sub _build_limiter {
    my $self = shift;
    #print "Worker limit is $limit\n";
    return Thread::Semaphore->new($self->limit);
}

sub _build_workers {
    return {};
}

sub _build_progress {
    my $self = shift;
    return Term::ProgressBar->new({
            count => $self->total,
            name => $self->name,
            ETA => 'linear',
            fh => \*STDOUT
        });
}

sub _build_done {
    return 0;
}

sub _build_next_progress_update {
    return 0;
}

# Submit a job with arguments.
# Return identifier of a spawned job or undef if error occured.
# Blocks until the job can be spawned.
sub schedule {
    my ($self, $job, @args) = @_;
    #print "A Job (@args) submitted into scheduler\n";
    confess "$job must be reference to CODE" unless(ref $job eq 'CODE');

    $self->limiter->down;
    my $thread = threads->create(\&slave, $self, $job, @args);

    if (! defined $thread) {
        carp "Could not spawn thread for job (@args): $!\n" . 
            "Decreasing worker limit by one\n";
        return undef;
    } else {
        #print "Job (@args) spawned\n";
        my $tid = $thread->tid;
        ${$self->workers}{$tid} = undef;
        return $tid;
    }
}

# Run a job and return reference to array [return value of job, exception $@].
# Return value of job will be undefined in case of defined exception.
sub slave {
    my ($self, $job, @args) = @_;
    my $retval;
    eval {
        $retval = &{$job}(@args);
    };
    $self->limiter->up;
    if ($@) {
        #print "A job (@args) died with $@\n";
        return [undef, $@];
    }
    #print "A job (@args) terminated with $retval\n";
    return [$retval, $@];
}

# If argument is true collects finished jobs only,
# otherwise blocks and waits for unfinished workers.
# Return job exit states [retval, exception] hashed by worker ID.
sub finish {
    my ($self, $non_blocking) = @_;
    my @threads;
    my %finished;
    if ((defined $non_blocking) && $non_blocking) {
        #print "Gathering finished jobs...\n";
        @threads = threads->list(threads::joinable);
    } else {
        #print "Waiting for unfinished jobs\n";
        @threads = threads->list();
    }
    # XXX: take care only about threads spawned by $self instance to allow
    # concurent class instances.
    foreach my $thread (grep { exists ${$self->workers}{$_->tid} } @threads) {
        #print "Waiting on thread " . $thread->tid . "...\n";
        my $retval = $thread->join;
        #print "Thread " . $thread->tid . " terminated with $retval.\n";
        $finished{$thread->tid} = $retval;

        $self->done($self->done() + 1);
        if ($self->total > 0 && $self->done >= $self->next_progress_update) {
            $self->next_progress_update($self->progress->update($self->done));
        }
    }
    return %finished;
}

# Collects finished jobs only.
# Return job exit states hashed by worker ID.
sub finished {
    my $self = shift;
    return $self->finish(1);
}

1;


