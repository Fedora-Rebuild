package Fedora::Rebuild::Execute;
use strict;
use warnings;
use version 0.77; our $VERSION = version->declare("v0.12.1");

use Moose;
use Carp;
use Proc::SyncExec;
use POSIX;
use Data::Dumper;
use namespace::clean;
use IO::Handle;

# TODO: Make StateLock subclass of this.

has logfd => (
    is => 'ro',
    isa => 'FileHandle',
    lazy_build => 1,
    init_arg => undef
);

sub _build_logfd {
    my $self = shift;
    return \*STDOUT;
}

# Print array into log
sub log {
    shift->logfd->print(@_);
}

# Fsync and close log file. Croaks on error.
sub close_log {
    my $self=shift;
    $self->logfd->sync
}

# Convert ${^CHILD_ERROR_NATIVE} to string description.
# XXX: This is not a method.
sub child_error_as_string {
    my $reason = ${^CHILD_ERROR_NATIVE};
    if (WIFEXITED($reason)) {
        $reason = "exit code " . WEXITSTATUS($reason);
    } elsif (WIFSIGNALED($reason)) {
        $reason = "signal " . WTERMSIG($reason);
    }
    return $reason;
}

# Format array of command with argument as quoted string
# XXX: This not a method
sub format_command {
    $Data::Dumper::Indent=0;
    $Data::Dumper::Terse=1;
    return '(' . join(' ', map {Dumper($_)} @_) . ')';
}

# Run command while appending output to log. Blocks. If workdir is nonempty
# string, switch into it before execution (and opening the log).
# Return true if command succeeds.
sub do {
    my ($self, $workdir, @command) = @_;

    my $redirect = sub {
        open(STDOUT, '>&', $self->logfd->fileno) and
        open(STDERR, '>&STDOUT');
        $self->log("Executing: " . format_command(@command) . "\n");
        if (defined $workdir && $workdir ne '' && !chdir $workdir) {
            $self->log("Could not change directory to $workdir: $!\n");
            return 0;
        }
        return 1;
    };
    my $pid = Proc::SyncExec::sync_exec($redirect, @command);
    if (!defined $pid) {
        $self->log("Could not execute " . format_command(@command) .  ": $!\n");
        return 0;
    }
    if ($pid != waitpid($pid, 0) || $?) {
        $self->log("Command " . format_command(@command) . " failed: " .
            child_error_as_string . "\n");
        return 0;
    }
    $self->log("Command " . format_command(@command) .
        " returned successfully.\n");
    return 1;
}

# Run command while appending stderr and stdout to log and stdout to refered
# output argument. In case of empty command output fill empty string;
# Blocks. If workdir is nonempty string, switch into it befere execution
# (and opening the log).
# Return true if command succeeds.
sub dooutput {
    my ($self, $workdir, $output, @command) = @_;

    my ($parent, $child);
    if (!pipe $child, $parent) {
        $self->log("Could not get connected pipes for command " .
            format_command(@command) . ": $!\n");
        return 0;
    }

    my $redirect = sub {
        close $child and
        open(STDOUT, '>&', fileno $parent) and
        close $parent and

        open(STDERR, '>&', $self->logfd->fileno) and
        $self->log("Executing: " . format_command(@command) . "\n");
        if (defined $workdir && $workdir ne '' && !chdir $workdir) {
            $self->log("Could not change directory to $workdir: $!\n");
            return 0;
        }
        return 1;
    };
    my $pid = Proc::SyncExec::sync_exec($redirect, @command);
    {
        my $errno = $!;
        close $parent;
        $! = $errno;
    }
    if (!defined $pid) {
        $self->log("Could not execute " . format_command(@command) . ": $!\n");
        return 0;
    }

    for ($$output = ''; local $_ = <$child>;) {
        $$output .= $_;
        $self->lograw($_);
    }

    if ($pid != waitpid($pid, 0) || $?) {
        $self->log("Command " . format_command(@command) . " failed: " .
            child_error_as_string . "\n");
        return 0;
    }

    $self->log("Command " . format_command(@command) .
        " returned successfully.\n");
    return 1;
}

1;
