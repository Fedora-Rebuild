package Fedora::Rebuild::RPM;
use strict;
use warnings;
use Moose;
use RPM2;
use RPM::VersionCompare;
use namespace::clean;

use version 0.77; our $VERSION = version->declare("v0.12.1");

# FLAGS defined in <rpm/rpmds.h> in enum rpmsenseFlags_e:
use constant {
    ANY => 0x0,
    LESS => 0x2,
    GREATER => 0x4,
    EQUAL => 0x8
};

# Return string representation of dependency FLAG qualifier (LESS|EQUAL, ...).
# ANY is represented as an asterisk. Higher bits of flag are ignored.
# XXX This is not a method.
sub flag_as_string {
    my $flag = $_[0];
    my $text = '';

    if ($flag & LESS) { $text .= '<'; }
    if ($flag & GREATER) { $text .= '>'; }
    if ($flag & EQUAL) { $text .= '='; }
    if ($text eq '') { $text = '*'; }

    return $text;
}

# Add $needles dependecies into $stash dependecies. The $stash will be
# modified. It does not compact overlapping dependencies because provides and
# requires have opposite ordering. It removes identical dependencies only.
# The added dependency is not copied deeply. All references remain unchaged.
# Hash format is { NAME1 => [ [FLAG1, VERSION1], [FLAG2, VERSION2] ] }.
# XXX: This is not a method.
sub adddeps {
    my ($stash, $needles) = @_;
    for my $needle (keys %{$needles}) {
        if (exists $$stash{$needle}) {
            my @subneedles = @{$$needles{$needle}};
            my @substraws = @{$$stash{$needle}};
            NEEDLE: for (my $n = 0; $n <= $#subneedles; $n++) {
                STRAW: for (my $s = 0; $s <= $#substraws; $s++) {
                    if ($subneedles[$n][0] eq $substraws[$s][0] &&
                        $subneedles[$n][1] eq $substraws[$s][1]) {
                        next NEEDLE;
                    }
                }
                push @{$$stash{$needle}}, $subneedles[$n];
            }
        } else  {
            $$stash{$needle} = $$needles{$needle};
        }
    }
}

# Check whether a required version constrain (flag, version) is fulfilled by
# a provided version constrain (flag, version). Return true or false (even in parser error).
# XXX: FLAG cannot be LESS|GREATER. Not implemented.
# XXX: This is not a method.
sub version_is_satisfied {
    my ($rflag, $rversion, $pflag, $pversion) = @_;

    if (($rflag & (LESS|GREATER|EQUAL)) == ANY ||
        ($pflag & (LESS|GREATER|EQUAL)) == ANY) {
        return 1;
    }

    my $order = RPM::VersionCompare::labelCompare($rversion, $pversion);
    if (!defined $order) {
        # Parser error;
        return 0;
    }

    # Satisfaction matrix:
    #
    # (labelCompare($rversion,$pversion) = $order) == 0:
    #           pflag
    #           <   <=  ==  >=  >
    # rflag <   1   1   0   0   0
    #       <=  1   1   1   1   0
    #       ==  0   1   1   1   0
    #       =>  0   1   1   1   1
    #       >   0   0   0   1   1
    #
    if ($order == 0) {
        if (($rflag & EQUAL) && ($pflag & EQUAL)) { return 1; }
        if (($rflag & LESS) && ($pflag & LESS)) { return 1; }
        if (($rflag & GREATER) && ($pflag & GREATER)) { return 1; }
    }

    # (labelCompare($rversion,$pversion) = $order) > 0:
    #           pflag
    #           <   <=  ==  >=  >
    # rflag <   1   1   1   1   1 
    #       <=  1   1   1   1   1
    #       ==  0   0   0   1   1
    #       =>  0   0   0   1   1
    #       >   0   0   0   1   1
    #
    if ($order > 0) {
        if (($rflag & LESS) || ($pflag & GREATER)) { return 1; }
    }

    # (labelCompare($rversion,$pversion) = $order) < 0:
    #           pflag
    #           <   <=  ==  >=  >
    # rflag <   1   1   0   0   0 
    #       <=  1   1   0   0   0
    #       ==  1   1   0   0   0
    #       =>  1   1   1   1   1
    #       >   1   1   1   1   1
    #
    if ($order < 0) {
        if (($rflag & GREATER) || ($pflag & LESS)) { return 1; }
    }

    return 0;
}


# Check whether a requires tripplet (name, flag, version) is fulfilled by
# hash of provides. Return true or false (even in parser error).
# Hash format is { NAME1 => [ [FLAG1, VERSION1], [FLAG2, VERSION2] ] }.
# XXX: FLAG cannot be LESS|GREATER. Not implemented.
# XXX: This is not a method.
sub is_satisfied {
    my ($rname, $rflag, $rversion, $provides) = @_;

    if (! (exists $$provides{$rname})) {
        return 0;
    }

    for my $provide (@{$$provides{$rname}}) {
        my $pflag = $$provide[0];
        my $pversion = $$provide[1];

        if (version_is_satisfied($rflag, $rversion, $pflag, $pversion)) {
            return 1;
        }
    }

    return 0;
}


has 'name' => (is => 'ro', isa => 'Str', required => 1);


# Load local RPM package and return the Header object and package ENVR
sub open_package {
    my $self = shift;

    my $headers;
    if (!eval { $headers = RPM2->open_package($self->name); }) {
        $@ = "Could not load RPM package `" . $self->name . "': ". $@;
        return undef;
    }

    my $envr = $headers->as_nvre;
    if (! defined $envr) {
        $@ = "Could not get ENVR of RPM package`" . $self->name . "'";
        return undef;
    }

    return ($headers, $envr);
}


# Destile provides from local RPM package.
# Return reference to hash of provides and ENVR string of the package in this
# order.
# In case of failure, return undef and set $@.
# Hash format is { NAME1 => [ [FLAG1, VERSION1], [FLAG2, VERSION2] ] }.
sub provides {
    my $self = shift;

    my ($headers, $envr) = $self->open_package();
    if (! defined $headers || ! defined $envr) {
        return undef;
    }

    my @names = $headers->tag('PROVIDENAME');
    my @flags = $headers->tag('PROVIDEFLAGS');
    my @versions = $headers->tag('PROVIDEVERSION');
    if (!($#names == $#flags) && ($#names == $#versions)) {
        $@ = "Inconsistent number of provides names, flags, and versions in `"
            . $self->name . "' RPM package";
        return undef;
    }

    my %provides;
    while (
            my $name = shift @names, 
            my $flag = shift @flags,
            defined(my $version = shift @versions)
    ) {
        # ???: Filter some higher flag bits?
        if (!exists $provides{$name}) { $provides{$name} = []; }
        push @{$provides{$name}}, [$flag, $version];
    }

    return (\%provides, $envr);
}


# Destile requires from local RPM package.
# Return reference to hash of requires or in case of failure, return undef
# and set $@.
# Hash format is { NAME1 => [ [FLAG1, VERSION1], [FLAG2, VERSION2] ] }.
sub requires {
    my $self = shift;

    my ($headers, $envr) = $self->open_package();
    if (! defined $headers || ! defined $envr) {
        return undef;
    }

    my @names = $headers->tag('REQUIRENAME');
    my @flags = $headers->tag('REQUIREFLAGS');
    my @versions = $headers->tag('REQUIREVERSION');
    if (!($#names == $#flags) && ($#names == $#versions)) {
        $@ = "Inconsistent number of requires names, flags, and versions in `"
            . $self->name . "' RPM package";
        return undef;
    }

    my %requires;
    while (
            my $name = shift @names, 
            my $flag = shift @flags,
            defined(my $version = shift @versions)
    ) {
        # ???: Filter some higher flag bits?
        if (!exists $requires{$name}) { $requires{$name} = []; }
        push @{$requires{$name}}, [$flag, $version];
    }

    return (\%requires, $envr);
}

1;
