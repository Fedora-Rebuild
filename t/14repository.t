#!/usr/bin/perl
use strict;
use warnings;
use Test::More tests => 9;

use Fedora::Rebuild::Repository;
use File::Temp ();
use File::Spec ();
use HTTP::Tiny ();
use URI ();
use URI::Escape ();

my $directory = File::Temp::tempdir(CLEANUP => 1);
ok($directory, 'temporary directory created');

my $repository = Fedora::Rebuild::Repository->new(path=>$directory);
ok($repository, 'Fedora::Rebuild::Repository object created');

my $url = $repository->start;
ok($url, 'repository server started');

my $file_name = 'c++';
my $file_content = '42';
my $file = File::Spec->catfile($directory, $file_name);
ok(open(my $fh, '>', $file), "file '$file_name' created");
print $fh $file_content;
ok(close($fh), "content written");

my $file_name_escaped = URI::Escape::uri_escape_utf8($file_name);
my $response = HTTP::Tiny->new->get(URI->new_abs($file_name_escaped, $url));
ok($response, "GET request for escaped '$file_name_escaped' sent");

ok($response->{success},
    "file '$file_name' retrieved: $response->{status} $response->{reason}");
is($response->{content}, $file_content, 'file content matches');

$repository->stop;
pass('repository server stopped');
