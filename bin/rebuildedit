#!/usr/bin/perl
use strict;
use warnings;
use Carp;
use Config::Tiny;
use Getopt::Long;
use Fedora::Rebuild::Set::Package;
use Fedora::Rebuild::Package;
use Fedora::Rebuild::Package::Config;
use Fedora::Rebuild::Scheduler;
use Fedora::Rebuild::Package::ListLog;
use Fcntl;
use File::Spec;
use File::Temp;

=encoding utf8

=head1 NAME

rebuildedit - Mass edit source repositories

=head1 SYNOPIS

    rebuildedit [--config FILE]

=head1 DESCRIPTION

This tool clones source repositories for all packages sepcified in I<all>
list, executes an I<editor> command there and if the repository is changed,
commits and pushed the changes back to the server.

Packages without changes or successfully changed ones will be logged into
I<done> list. Packages that encountered an error will be logged into <failed>
list.

The I<editor> is a list consisting of command name folloed by optional
positional arguments. When executing the editor command, a spec file will be
appended as a last argument. The editor will be executed in the git repository
directory.

=cut

my $cfgfile = File::Spec->catfile($ENV{HOME}, '.rebuildperlrc');
my %config = (
    all => undef,
    done => undef,
    failed => undef,
    workdir => undef,
    repodir => undef,
    pyrpkg => 'fedpkg',
    mode => 'koji',
    anonymous => 0,
    dist => undef,
    loadthreads => 1,
    target => '',
    message => '',
    editor => undef,
);

=head1 OPTIONS

=head2 --config I<FILE>

Read configuration from I<FILE>, or F<~/.rebuildperlrc> if not specified.

=head1 FILES

=head2 F<~/.rebuildperlrc>

Configuration is in L<Config::Tiny> format. Following options are needed:

    done = done
    failed = failed
    workdir = workdir
    dist = rawhide
    editor = [ 'program', 'argument' ]
    message = A commit mesage

Additional possible options:

    mode => koji
    anonymous => 0
    loadthreads = 1

=cut


GetOptions(
    'config=s' => \$cfgfile,
) or die "Could not parse program options\n";

# Load configuration
if (-f $cfgfile) {
    my $cfg = Config::Tiny->new->read($cfgfile);
    if (! defined $cfg) {
        print STDERR "Could not parse `" . $cfgfile .
            "' configuration file: " . $Config::Tiny::errstr . "\n";
        exit 1;
    }
    foreach (keys %{$cfg->{_}}) {
        $config{$_} = $cfg->{_}->{$_};
        $config{$_} = eval $config{$_} if $_ eq 'editor';
    }
}

# Load list of packages
sub load_list {
    my ($file_name) = @_;
    my $packages = Fedora::Rebuild::Set::Package->new();
    my $file;

    if (not -e $file_name) {
        return $packages;
    }

    open($file, '<', $file_name) or
        croak "Could not open `" . $file_name . "': $!";
    while (local $_ = <$file>) {
        chomp;
        if (m/^\s*$/) { next; }
        if ($packages->contains($_)) {
            warn "Duplicate name `$_'.\n";
            next;
        }

        my $package = Fedora::Rebuild::Package->new(name => $_,
            workdir => $config{workdir}, dist => $config{dist},
            pkgdist => $config{pkgdist}, target => $config{target},
            message => $config{message});
        $packages->insert($package);
    }
    if ($!) {
        croak "Could not read list of package names from file `" .
        $file_name . "': $!";
    }
    close $file;

    return $packages;
}

# Build config

my $build_config = Fedora::Rebuild::Package::Config->new(
    mode => $config{mode},
    pyrpkg => $config{pyrpkg},
    koji => 'koji',
    repositories => [],
    architecture => 'x86_64',
    mock_install_packages => '',
);


# Load package lists
print "Loading list of all package names...\n";
my $packages = load_list($config{all});
print "Number of all packages: " . $packages->size() . "\n";

print "Loading list of done package names...\n";
my $done = load_list($config{done});
print "Number of done packages: " . $done->size() . "\n";

print "Loading list of failed package names...\n";
my $failed = load_list($config{failed});
print "Number of failed packages: " . $failed->size() . "\n";

# Strip done and failed packages
for ($done->packages, $failed->packages) {
    $packages->delete($_);
}

# Process packages
my $scheduler = Fedora::Rebuild::Scheduler->new(
    limit => $config{loadthreads},
    name => 'Editing repository',
    total => $packages->size
);
my %jobs = ();
my $i = 0;

for my $package ($packages->packages) {
    my $job = $scheduler->schedule($package->can('cloneeditpush'), $package,
        $build_config, $config{anonymous}, $config{editor});
    if (! defined $job) {
        next;
    }
    $jobs{$job} = $package;
    my %finished = $scheduler->finish(++$i < $packages->size);

    while (my ($job, $status) = each %finished) {
        my $package = $jobs{$job};

        if (!$$status[0]) {
            print "Could not edit repository for package `",
                 $package->name, "'.\n";
            Fedora::Rebuild::Package::ListLog->new(
                file => $config{failed})->log($package);
        } else {
            Fedora::Rebuild::Package::ListLog->new(
                file => $config{done})->log($package);
        }
    }
}

print "Done.\n";
exit 0;

