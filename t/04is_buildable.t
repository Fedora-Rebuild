#!/usr/bin/perl
use strict;
use warnings;

use Test::Simple tests => 8;

use Fedora::Rebuild::Solver;
use Fedora::Rebuild::Set::Package;
use Fedora::Rebuild::Package;
use Fedora::Rebuild::RPM;
use threads::shared;

# Prepare packages
# Create and return package with one binary package that always provides its
# unversioned name.
sub build_package {
    my ($name, $provides, $runrequires, $buildrequires) = @_;

    my $package = Fedora::Rebuild::Package->new(
        'name' => $name,
        workdir => 't/tmp',
        dist => 'foo',
        target => 'foo',
        message => 'foo',
    );

    $package->provides(shared_clone(
            {
                $name => {
                    $name => [[Fedora::Rebuild::RPM::ANY, '']],
                    %$provides
                }
            }
        ));
    $package->runrequires(shared_clone(
            { $name => $runrequires }
        ));
    $package->requires(shared_clone($buildrequires));
    return $package;
}

my $B1 = build_package(
    'B1',
    {},
    {},
    { 'a' => [[Fedora::Rebuild::RPM::ANY, '']] }
);

my $B2 = build_package(
    'B2',
    {},
    {},
    {'a' => [[Fedora::Rebuild::RPM::ANY, '']]}
);

my $C = build_package(
    'C',
    {
        'dual' =>
            [[Fedora::Rebuild::RPM::EQUAL|Fedora::Rebuild::RPM::GREATER, '1']],
    },
    {},
    { 'nonexistent' => [[Fedora::Rebuild::RPM::ANY, '']] }
);

my $a = build_package(
    'a',
    {
        'dual' =>
            [[Fedora::Rebuild::RPM::EQUAL|Fedora::Rebuild::RPM::GREATER, '1']],
    },
    {},
    {}
);

my $b1 = build_package(
    'b1',
    {},
    { 'a' => [[Fedora::Rebuild::RPM::ANY, '']] },
    {},
);

my $b2 = build_package(
    'b2',
    {},
    {'a' => [[Fedora::Rebuild::RPM::ANY, '']]},
    {},
);

my $c = build_package(
    'c',
    {
        'dual' =>
            [[Fedora::Rebuild::RPM::EQUAL|Fedora::Rebuild::RPM::GREATER, '1']],
    },
    { 'nonexistent' => [[Fedora::Rebuild::RPM::ANY, '']] },
    {},
);

my $D = build_package(
    'D',
    {},
    {},
    {'b1' => [[Fedora::Rebuild::RPM::ANY, '']]},
);

my $E = build_package(
    'E',
    {},
    {},
    {
        'b1' => [[Fedora::Rebuild::RPM::ANY, '']],
        'b2' => [[Fedora::Rebuild::RPM::ANY, '']],
    },
);

my $F = build_package(
    'F',
    {},
    {},
    {
        'a' => [[Fedora::Rebuild::RPM::ANY, '']],
        'dual' => [[Fedora::Rebuild::RPM::ANY, '']],
    },
);

my $G = build_package(
    'G',
    {},
    {},
    {
        'dual' => [
            [Fedora::Rebuild::RPM::GREATER, '1'],
            [Fedora::Rebuild::RPM::LESS, '1'],
            [Fedora::Rebuild::RPM::GREATER, '2'],
        ]
    },
);

my $H = build_package(
    'H',
    {},
    {},
    { 'c' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

# Prepare set of packages
my $packages = Fedora::Rebuild::Set::Package->new();
$packages->insert($B1);
$packages->insert($B2);
$packages->insert($C);
$packages->insert($a);
$packages->insert($b1);
$packages->insert($b2);
$packages->insert($c);
$packages->insert($D);
$packages->insert($E);
$packages->insert($F);
$packages->insert($G);
$packages->insert($H);

# Prepare solver
my $solver = Fedora::Rebuild::Solver->new(packages => $packages,
    dependencyfilter => sub {0});

# Test
my $message;

# Direct dependencies
ok($solver->is_buildable($a, \$message),
    "a build-requires nothing: $message");
ok($solver->is_buildable($B1, \$message),
    "B1 build-requires a: $message");
ok(! $solver->is_buildable($C, \$message),
    "C build-requires nonexistent: $message");

# Indirect dependencies
ok($solver->is_buildable($D, \$message),
    "D build-requires b1 that requires a: $message");
ok($solver->is_buildable($E, \$message),
    "E build-requires b1 and b2, both require a: $message");
ok(! $solver->is_buildable($H, \$message),
    "H build-requires c that is not installable: $message");

# More provides, one is not installable.
# XXX: This is not deterministic as it depend on hash ordering.
ok($solver->is_buildable($F, \$message),
    "F build-requires a and dual, dual is provided by installable a and " .
    "uninstallable c: $message");

# More provides, one is not installable, but all requires cannot be satisfied
# at the same time.
ok(! $solver->is_buildable($G, \$message),
    "G build-requires versioned dual's, they cannot be satisfied at the " .
    "same time: $message");
