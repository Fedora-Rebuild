package Fedora::Rebuild::Package;
use strict;
use warnings;
use threads;
use threads::shared;
use Moose;
use File::Path::Tiny;
use File::Spec;
use Fedora::Rebuild::Package::StateLock;
use Fedora::Rebuild::RPM;
use Fedora::Rebuild::Mock;
use Data::Dumper;
use namespace::clean;

use version 0.77; our $VERSION = version->declare("v0.12.1");

has 'name' => (is => 'ro', isa => 'Str', required => 1);
# Build-time dependencies
has 'requires' => (is => 'rw', isa => 'HashRef', lazy_build => 1,
    init_arg => undef);
# Run-time dependencies hashed by binary package ENVR.
has 'runrequires' => (is => 'rw', isa => 'HashRef', lazy_build => 1,
    init_arg => undef);
# Run-time provides hashes by binary package ENVR.
has 'provides' => (is => 'rw', isa => 'HashRef', lazy_build => 1,
    init_arg => undef);
has 'workdir' => (is => 'ro', isa => 'Str', required => 1);
# Git branch name
# "f14", "f15" etc. Use "rawhide" for latest one.
has 'dist' => (is => 'ro', isa => 'Str', required => 1);
# Fedpkg dist name identifier. This is required only if git branch name is not
# an offical one (e.g. a private branch).
# "f14", "f15" etc. Use "f18" for the latest one.
has 'pkgdist' => (is => 'ro', isa => 'Maybe[Str]', required => 0,
    default => undef);
# Build target name
# "dist-f14", "dist-f15" etc. Use "rawhide" for latest one.
has 'target' => (is => 'ro', isa => 'Str', required => 1);
has 'message' => (is => 'ro', isa => 'Str', required => 1);

has 'packagedir' => (is => 'ro', isa => 'Str', lazy_build => 1,
    init_arg => undef);
has 'repodir' => (is => 'ro', isa => 'Str', lazy_build => 1,
    init_arg => undef);
has 'mockdir' => (is => 'ro', isa => 'Str', lazy_build => 1,
    init_arg => undef);
has 'rpmdir' => (is => 'ro', isa => 'Str', lazy_build => 1,
    init_arg => undef);
has 'requiresstore' => (is => 'ro', isa => 'Str', lazy_build => 1,
    init_arg => undef);
has 'runrequiresstore' => (is => 'ro', isa => 'Str', lazy_build => 1,
    init_arg => undef);
has 'providesstore' => (is => 'ro', isa => 'Str', lazy_build => 1,
    init_arg => undef);
has 'taskstore' => (is => 'ro', isa => 'Str', lazy_build => 1,
    init_arg => undef);
has 'branch' => (is => 'ro', isa => 'Str', lazy_build => 1,
    init_arg => undef);

# Make object shared between threads.
# XXX: Not all attributes are shared automatically.
around 'new' => sub {
    my $orig = shift;
    my $class = shift;
    return shared_clone($class->$orig(@_));
};

# Clean package locks bound to attributes
sub BUILD {
    my $self = shift;
    for my $state ('buildrequires', 'runrequires', 'provides') {
        Fedora::Rebuild::Package::StateLock->new(package => $self,
            state => $state)->mark_failed;
    }
}

# BuildRequires. Shared.
# Initialize to empty hash. Call get_buildrequires() to populate.
sub _build_requires {
    my $self = shift;
    my $requires :shared = shared_clone({});
    return $requires;
}

# Run-time Requires. Shared.
# Initialize to empty hash. Call get_runrequires() to populate.
sub _build_runrequires {
    my $self = shift;
    my $runrequires :shared = shared_clone({});
    return $runrequires;
}

# Run-time provides. Shared.
# Initialize to empty hash. Call rebuild() or get_binaryprovides() to populate.
sub _build_provides {
    my $self = shift;
    my $provides :shared = shared_clone({});
    return $provides;
}

sub _build_packagedir {
    my $self = shift;
    my $directory = File::Spec->catfile($self->workdir, $self->name);
    if (! -d $directory) {
        File::Path::Tiny::mk($directory) or
            die "Could not create directory $directory: $!";
    }
    return $directory;
}

sub _build_repodir {
    my $self = shift;
    return File::Spec->catfile($self->packagedir, 'repository');
}

sub _build_mockdir {
    my $self = shift;
    return File::Spec->catfile($self->packagedir, 'mock');
}

sub _build_rpmdir {
    my $self = shift;
    return File::Spec->catfile($self->packagedir, 'RPMS');
}

sub _build_requiresstore {
    my $self = shift;
    return File::Spec->catfile($self->packagedir, 'buildrequires.store');
}

sub _build_runrequiresstore {
    my $self = shift;
    return File::Spec->catfile($self->packagedir, 'runrequires.store');
}

sub _build_providesstore {
    my $self = shift;
    return File::Spec->catfile($self->packagedir, 'provides.store');
}

sub _build_taskstore {
    my $self = shift;
    return File::Spec->catfile($self->packagedir, 'task.store');
}

sub _build_branch {
    my $self = shift;
    if ($self->dist eq 'rawhide') {
        return 'master';
    } else {
        return $self->dist;
    }
}


# Clones package repository and switch to proper branch if not yet done.
# First argument is anonymous clone boolean.
# Second argument is Fedora::Rebuild::Package::Config object.
# Return true on success.
sub clone {
    my ($self, $anonymous, $build_config) = @_;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'clone');
    if ($lock->is_done) { return 1; }

    # XXX: pyrpkg creates subdirectory named like package. The package name
    # could clash with our file structure. Thus we need to clone into secure
    # directory and move repository content to fixed name $self->repodir after
    # that.

    if (-d $self->repodir) { File::Path::Tiny::rm($self->repodir); }
    my $tempdir = File::Spec->catfile($self->packagedir, 'tmp');
    my $temprepodir = File::Spec->catfile($tempdir, $self->name);
    if (-d $tempdir) { File::Path::Tiny::rm($tempdir); }
    if (!File::Path::Tiny::mk($tempdir)) {
        $lock->log("Could not create directory `" . $tempdir . "': $!\n");
        return $lock->mark_failed;
    }

    if (!$lock->do($tempdir, $build_config->pyrpkg(), 'clone',
            ($anonymous) ? '-a': (), $self->name)) {
        $lock->log("Could not clone `" . $self->name . "' repository.\n");
        return $lock->mark_failed;
    }

    if (!rename($temprepodir, $self->repodir)) {
        $lock->log("Could not move `" . $temprepodir . "' content to to `" .
            $self->repodir . "'.\n");
        return $lock->mark_failed;
    }
    File::Path::Tiny::rm($tempdir);

    if (!$lock->do($self->repodir, $build_config->pyrpkg(),
            'switch-branch', $self->branch)) {
        $lock->log("Could not switch `" . $self->name .
            "' repository to branch `" . $self->branch . "'.\n");
        return $lock->mark_failed;
    }

    return $lock->mark_done;
}


# Reset git repository and pull new changes.
# First argument is a state lock.
# XXX: The state is not marked as failed in case of error,
# Return true on success.
# Needs `clone'.
sub refreshgit {
    my ($self, $lock) = @_;
    if (!defined $lock) {
        return 0;
    }

    # Reset git repository
    if (!$lock->do($self->repodir, 'git', 'reset', '--hard',
            'origin/' .  $self->branch)) {
        $lock->log("Could not reset git repository in `" . $self->repodir .
            "'.\n");
        return 0;
    }
   
    # Pull git repository
    if (!$lock->do($self->repodir, 'git', 'pull')) {
        $lock->log("Could not pull git repository in `" . $self->repodir .
            "'.\n");
        return 0;
    }
   
    return 1;
}


# Increase package revision with proper changelog entry. Do not commit nor
# push the changes.
# First argument is a state lock.
# XXX: The state is not marked as failed in case of error,
# Return true on success.
# Needs `clone'.
sub updatespec {
    my ($self, $lock) = @_;
    if (!defined $lock) {
        return 0;
    }

    # Reset and pull git repository
    if (!$self->refreshgit($lock)) {
        return 0;
    }

    # Increase revision number
    my $specfile = File::Spec->catfile($self->repodir, $self->name . '.spec');
    if (!$lock->do(undef, 'rpmdev-bumpspec', '-c', $self->message, $specfile)) {
        $lock->log("Could not increase revison number in `" . $specfile .
            "' specfile.\n");
        return 0;
    }

    return 1;
}


# Check if git repository is modifed (has uncommited changes).
# First arugument is a state lock.
# Second argument is a mode.
# Return -1 on an internal error.
# Return true if modifer, false otherwise.
# Needs `clone'.
sub gitismodified {
    my ($self, $lock) = @_;

    my $output;
    if (!$lock->dooutput($self->repodir, \$output, 'git', 'status',
            '--porcelain', '--untracked-files=no')) {
        return -1;
    }
    if (defined $output and $output ne '') {
        return 1;
    }

    return 0;
}


# Commit changes. And push them only if first argument is "koji".
# First arugument is a state lock.
# Second argument is a mode.
# Return true on success.
# Needs `clone'.
sub commitandpush {
    my ($self, $lock, $mode) = @_;

    # Commit changes
    if (!$lock->do($self->repodir, 'git', 'commit', '-a',
            '-m', $self->message)) {
        $lock->log("Could not commit changes into git repository `" .
            $self->repodir . "'.\n");
        return 0;
    }

    if ($mode eq 'koji') {
        # Push changes
        if (!$lock->do($self->repodir, 'git', 'push')) {
            $lock->log("Could not push changes from repository `" .
                $self->repodir . "' to server.\n");
            return 0;
        }
    } else {
        $lock->log("Not pushing changes because of local build mode.\n");
    }

    return 1;
}


# Remove all state locks except `clone'.
# XXX: The state is not marked as failed in case of error.
# Returns true.
sub reset {
    my ($self) = @_;

    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'reset');

    for my $state ('srpm', 'buildrequiresstore', 'edit', 'update',
        'is_rebuildable', 'submitbuild', 'build', 'rpms', 'providesstore',
        'runrequiresstore', 'rotate', 'buildrequires', 'runrequires',
        'provides') {
        $lock->log("Removing state lock $state\n");

        if ($state eq 'rotate') {
            $self->reset_rotate;
        } else {
            my $state_lock = Fedora::Rebuild::Package::StateLock->new(
                package => $self, state => $state);
            $state_lock->mark_failed;
            $state_lock->remove_log;
        }
    }
    $lock->log("States reset done.\n");

    # The lock is for logging only
    $lock->mark_failed;
    return 1;
}


# Test if remote GIT repository HEAD differs from local clone. If the package
# has been changed in the remote repository, it will reset all package
# states up to (excluding) `clone'.
# Return false on failure, 1 for no change (repositories equals),
# 2 for detected change.
# Needs `clone'.
sub reset_remotly_updated {
    my ($self) = @_;

    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'checkremote');

    if (!-d $self->repodir) {
        $lock->log("Missing repository directory `" . $self->repodir . "\n");
        return $lock->mark_failed;
    }

    # Expected output for "git show-ref origin/master"
    # bb7f5790c2ef49087abe175e8b9560acce4ae15d refs/remotes/origin/master
    my $local_id;
    my $branch_name = $self->branch;
    if (!$lock->dooutput($self->repodir, \$local_id, 'git', 'show-ref',
            'origin/' .  $self->branch)) {
        $lock->log("Could not get HEAD of local origin branch.\n");
        return $lock->mark_failed;
    }
    if (!defined $local_id or $local_id !~
            s|\A(\S+)\s+refs/remotes/origin/\Q$branch_name\E\n\z|$1|) {
        $lock->log("Could not parse local origin reference ID: `$local_id'\n");
        return $lock->mark_failed;
    }

    # Expected output for "git ls-remote origin heads/master"
    # 8d021a3956bbb1762ae52a04a5bc425a77c8a3ad        refs/heads/master
    # XXX: git ls-remote --heads ... handles master as a wildcard and returns
    # entry for rc/master branch too.
    my $remote_id;
    if (!$lock->dooutput($self->repodir, \$remote_id, 'git', 'ls-remote',
            'origin', 'heads/' . $self->branch)) {
        $lock->log("Could not get HEAD of remote origin branch.\n");
        return $lock->mark_failed;
    }
    if (!defined $remote_id or $remote_id !~
            s|\A(\S+)\s+refs/heads/\Q$branch_name\E\n\z|$1|) {
        $lock->log("Could not parse remote origin reference ID: `$remote_id'\n");
        return $lock->mark_failed;
    }

    my $changed = $local_id ne $remote_id;
    if ($changed) {
        $lock->log("Remote repository has changed.\n");
        $self->reset();
    } else {
        $lock->log("Remote repository has not changed.\n");
    }

    # The lock is for logging only
    $lock->mark_failed;

    if ($changed) {
        return 2;
    }
    return 1;
}


# Builds SRPM. Return true on success.
# If first argument is true, recreate SRPM forcefully.
# Second argument is Fedora::Rebuild::Package::Config object.
# Third argument is Mock object to use instead of fresh one (value undef).
# (This is safe only in single-threaded run. This is limitation of mock.)
# Needs `clone'.
sub srpm {
    my ($self, $force, $build_config, $mock) = @_;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'srpm');
    if ($force) { $lock->remove_lock; }
    if ($lock->is_done) { return 1; }

    # We need do the update spec now to get the same NEVRA as in rebuild. We do
    # update without pushing to prevent from repetitive NEVRA bumps in case of
    # failed builds.
    if (!Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'update')->is_done) {
        if (!$self->updatespec($lock)) {
            $lock->log("Could not update spec file in order to create new " .
                "SRPM for `" . $self->name . "' package.\n");
            return $lock->mark_failed;
        }
    }

    if ($build_config->mode() eq 'local') {
        if (!$lock->do($self->repodir, $build_config->pyrpkg(),
                (defined $self->pkgdist) ? ('--dist', $self->pkgdist) : (),
                'srpm')) {
            $lock->log("Could not build SRPM for `" . $self->name .
                "' package locally.\n");
            return $lock->mark_failed;
        }
    } elsif ($build_config->mode() eq 'koji' or
            $build_config->mode() eq 'mock') {
        # Prepare spec file and sources
        my $specname = File::Spec->catfile($self->repodir,
            $self->name . '.spec');
        if (!$lock->do($self->repodir, $build_config->pyrpkg(),
                (defined $self->pkgdist) ? ('--dist', $self->pkgdist) : (),
                'sources')) {
            $lock->log("Could not download sources for `" . $self->name .
                "' package.\n");
            return $lock->mark_failed;
        }

        # Build SRPM in mock
        if (!replace_directory($lock, $self->mockdir)) {
            return $lock->mark_failed;
        }
        my ($mock_config_dir, $mock_config_root);
        eval {
            if (!defined $mock) {
                $mock = Fedora::Rebuild::Mock->new(
                        architecture => $build_config->architecture(),
                        repositories => $build_config->repositories(),
                        install_packages =>
                            $build_config->mock_install_packages(),
                );
                $mock->make_configuration;
            }
            $mock_config_dir = $mock->config_dir;
            $mock_config_root = $mock->config_root;
        };
        if ($@) {
            $lock->log("Could not configure mock: $@\n");
            File::Path::Tiny::rm($mock_config_dir);
            return $lock->mark_failed;
        }
        if ($mock->shared) {
            # Mock stopped to pruning _topdir and accumulates built SRPMs now
            # and die on final check for number of SRPM files.
            # Let clean it manually.
            if (!$lock->do(undef, 'mock', '--resultdir', $self->mockdir,
                    '--configdir', $mock_config_dir,
                    '--root', $mock_config_root,
                    '--no-clean', '--no-cleanup-after',
                    '--chroot',
                    q[find "$(rpm -E '%{_topdir}')" -mindepth 2 -maxdepth 2 -exec rm -r {} \;])) {
                $lock->log("Could not prune _topdir for `" . $self->name .
                    "' in mock.\n");
                return $lock->mark_failed;
            }
        }
        if (!$lock->do(undef, 'mock', '--resultdir', $self->mockdir,
                '--configdir', $mock_config_dir, '--root', $mock_config_root,
                ($mock->shared) ? ('--no-clean', '--no-cleanup-after') : (),
                '--buildsrpm',
                '--spec', $specname, '--sources', $self->repodir)) {
            $lock->log("Could not build SRPM for `" . $self->name .
                "' in mock.\n");
            if (!$mock->shared) {
                File::Path::Tiny::rm($mock_config_dir);
            }
            return $lock->mark_failed;
        }
        if (!$mock->shared) {
            File::Path::Tiny::rm($mock_config_dir);
        }

        # Move SRPM from mockdir to repodir
        my @srpms = glob(File::Spec->catfile($self->mockdir, '*.src.rpm'));
        if ($#srpms < 0) {
            $lock->log("No SRPM package found under `" .
                $self->mockdir . "'\n");
            return $lock->mark_failed;
        }
        if ($#srpms > 0) {
            $lock->log("More SRPM packages found under `" .
                $self->mockdir . "'. This should not happen.\n");
            return $lock->mark_failed;
        }
        if (!copy_files_into_directory($lock, $self->repodir, @srpms)) {
            return $lock->mark_failed;
        }
    } else {
        $lock->log("Could not build SRPM for `" . $self->name .
            "' because of unknown building mode `" . $build_config->mode() .
            "'.\n");
        return $lock->mark_failed;
    }

    return $lock->mark_done;
}

# Get current package NEVRA from sources in repository.
# First argument is state lock where process of getting NEVRA including
# potential failure is logged.
# Second argument is Fedora::Rebuild::Package::Config object.
# XXX: The state is not marked as failed in case of error,
# Return NEVRA string or undef in case of error.
sub get_nevra_from_git {
    my ($self, $lock, $build_config) = @_;

    my $nevra;
    if (!$lock->dooutput($self->repodir, \$nevra, $build_config->pyrpkg(),
        (defined $self->pkgdist) ? ('--dist', $self->pkgdist) : (),
        'verrel') ||
            $nevra eq '') {
        $lock->log("Could not get NEVRA from `" . $self->name .
            "' git repository package.\n");
        return undef;
    }
    chomp $nevra;
    # Consider last line only becuase of bug in pyrpkg
    # <https://bugzilla.redhat.com/show_bug.cgi?id=721389>.
    my @lines = (split qr{$/}, $nevra);
    $nevra = pop @lines;
    return $nevra;
}


# Get current package SRPM name.
# If the SRPM file does not exist, it will be re-created.
# First argument is state lock where process of building SRPM including
# potential failure is logged.
# Second argument is Fedora::Rebuild::Package::Config object. If mode value is:
#   "local"             build SRPM locally using pyrpkg,
#   "koji" or "mock"    build SRPM using mock.
# Third argument is Mock object to use instead of fresh one (value undef).
# XXX: The state is not marked as failed in case of error,
# Return SRPM file name string or undef in case of error.
sub get_srpm_name {
    my ($self, $lock, $build_config, $mock) = @_;

    my $nevra = $self->get_nevra_from_git($lock, $build_config);
    if (! defined $nevra) {
        return undef;
    }

    my $srpmname = File::Spec->catfile($self->repodir, $nevra . '.src.rpm');
    if (! -f $srpmname ) {
        $lock->log("SRPM package `" . $srpmname . "' is missing, " .
            "trying to create SRPM again...\n");
        if (!$self->srpm(1, $build_config, $mock)
                || ! -f $srpmname) {
            $lock->log("`Could not recreate SRPM package '" . $srpmname .
                "'.\n");
            return undef;
        }
    }
    return $srpmname;
}

# Remove a directory recursively, if it exists.
# First argument is the directory, second argument is lock to log errors into.
# Return true, false in case of error.
# XXX: This is not a method
sub remove_directory {
    my ($lock, $directory) = @_;
    if (!File::Path::Tiny::rm($directory)) {
        $lock->log("Could not remove directory `" . $directory . "': $!\n");
        return 0;
    }
    return 1;
}

# Create a directory. If it exists, it will remove it before.
# First argument is the directory, second argument is lock to log errors into.
# Return true, false in case of error.
# XXX: This is not a method
sub replace_directory {
    my ($lock, $directory) = @_;
    remove_directory($lock, $directory);
    if (!File::Path::Tiny::mk($directory)) {
        $lock->log("Could not create directory `" . $directory . "': $!\n");
        return 0;
    }
    return 1;
}

# Copy files into existing directory.
# First argument lock for logggin,
# second argument is destinatinon directory,
# The last is list of files to be copied.
# Return true in sucesss, false in case of error.
# XXX: This is not a method
sub copy_files_into_directory {
    my ($lock, $directory, @files) = @_;
    for my $file (@files) {
        use File::Copy;
        if (!copy($file, $directory)) {
            $lock->log("Could not copy `". $file . "' into `". $directory .
                "'\n");
            return 0;
        }
        $lock->log("`" . $file . "' copied into `" . $directory . "'\n");
    }
    return 1;
}

# Destile BuildRequires from local SRPM and serialize them into file.
# First argument is Fedora::Rebuild::Package::Config object. If mode member is:
#   "local"             build SRPM locally using pyrpkg,
#   "koji" or "mock"    build SRPM using mock.
# Second argument is Mock object to use instead of fresh one (value undef).
# Return true on success.
# Needs `srpm'.
# FIXME: does not work after cleaning clone or doing update.
sub storebuildrequires {
    my ($self, $build_config, $mock) = @_;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'buildrequiresstore');
    if ($lock->is_done) { return 1; }

    my $nevra = $self->get_nevra_from_git($lock, $build_config);
    if (! defined $nevra) {
        return $lock->mark_failed;
    }

    my $srpmname = File::Spec->catfile($self->repodir, $nevra . '.src.rpm');
    if (! -f $srpmname ) {
        $lock->log("SRPM package `" . $srpmname . "' is missing, " .
            "trying to create SRPM again...\n");
        if (!$self->srpm(1, $build_config, $mock) || ! -f $srpmname) {
            $lock->log("`Could not recreate SRPM package '" . $srpmname .
                "'.\n");
            return $lock->mark_failed;
        }
    }

    my $rpm = Fedora::Rebuild::RPM->new(name => $srpmname);
    my ($requires, $envra) = $rpm->requires;
    if (! defined $requires) {
        $lock->log("Could not get requires of SRPM `" . $srpmname . "': ". $@
            . "\n");
        return $lock->mark_failed;
    }

    if (! $lock->nstorereference($requires, $self->requiresstore)) {
        $lock->log("Could not store requires of SRPM `". $srpmname .
            "' into `" . $self->requiresstore . "' file: $@\n");
        return $lock->mark_failed;
    }

    $lock->log(Data::Dumper::Dumper($requires) . "\n");
    return $lock->mark_done;
}

# Destile BuildRequires from local SRPM. Return true on success.
# Needs `buildrequiresstore'.
sub buildrequires {
    my $self = shift;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'buildrequires');
    if ($lock->is_done) { return 1; }

    my $requires = $lock->retrievereference($self->requiresstore);
    if (! $requires) {
        $lock->log("Could not load requires of `". $self->name .
            "' package from `" . $self->requiresstore . "' file: $@\n");
        return $lock->mark_failed;
    }
    $self->requires(shared_clone($requires));

    $lock->log(Data::Dumper::Dumper($self->requires) . "\n");
    return $lock->mark_done;
}

# Record verdict from dependency solver whether the package is rebuildable.
# This step is always redone.
sub log_is_rebuildable {
    my ($self, $is_rebuildable, $message) = @_;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'is_rebuildable');
    $lock->remove_lock;

    if (defined $message) {
        $lock->log("Solver result for possibility of rebuilding SRPM for `" .
            $self->name . "': $message\n");
    }
    if (! $is_rebuildable) {
        $lock->log("According dependency solver, this package is not " .
            "rebuildable now.\n");
        return $lock->mark_failed;
    }

    $lock->log("According dependency solver, this package is " .
        "rebuildable now.\n");
    return $lock->mark_done;
}

# Get binary RPM packages for the source package.
# First argument is Fedora::Rebuild::Package::Config object. If mode member is:
#   'koji'  download them from Koji,
#   'local' from local build,
#   'mock'  from mock result directory.
# noarch packages are colletected automatically.
# Requires `clone'. Sould be called after `build'.
# Return true on success.
sub binaryrpm {
    my ($self, $build_config) = @_;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'rpms');
    if ($lock->is_done) { return 1; }

    if (!replace_directory($lock, $self->rpmdir)) {
        return $lock->mark_failed;
    }

    my @archs = ($build_config->architecture(), 'noarch');
    if ($build_config->mode eq 'koji') {
        $lock->log("Getting binary RPM packages from Koji:\n");

        my $nevra = $self->get_nevra_from_git($lock, $build_config);
        if (! defined $nevra) {
            return $lock->mark_failed;
        }

        # TODO: Get all archs, remove SRPM
        if (!$lock->do($self->rpmdir, $build_config->koji(), 'download-build',
                (map {'--arch=' . $_ } @archs), $nevra)) {
            $lock->log("Could not get binary RPM packages for `" . $nevra .
                "'\n");
            return $lock->mark_failed;
        }
    } elsif ($build_config->mode() eq 'local') {
        $lock->log("Getting binary RPM packages from local build:\n");

        my @rpms = map { glob(File::Spec->catfile($self->repodir, $_,
            '*.rpm')) } @archs;
        if ($#rpms < 0) {
            $lock->log("No binary RPM packages found under `" .
                $self->repodir . "'\n");
            return $lock->mark_failed;
        }

        if (!copy_files_into_directory($lock, $self->rpmdir, @rpms)) {
            return $lock->mark_failed;
        }
    } elsif ($build_config->mode() eq 'mock') {
        $lock->log("Getting binary RPM packages from mock build:\n");

        my @rpms = map { glob(File::Spec->catfile($self->mockdir,
            ('*.' . $_ . '.rpm'))) } @archs;
        if ($#rpms < 0) {
            $lock->log("No binary RPM packages found under `" .
                $self->mockdir . "'\n");
            return $lock->mark_failed;
        }

        if (!copy_files_into_directory($lock, $self->rpmdir, @rpms)) {
            return $lock->mark_failed;
        }
    } else {
        $lock->log("Could get binary RPM packages for `" . $self->name .
            "' source package because of unknown building mode `" .
            $build_config->mode() . "'\n");
        return $lock->mark_failed;
    }

    return $lock->mark_done;
}

# Return list of binary RPM files relative to current working directory.
# XXX: Should be called after colleting the files from build process by
# binaryrpm().
sub listbinaryrpmfiles {
    my $self = shift;
    return (glob(File::Spec->catfile($self->rpmdir, '*.rpm')));
}

# Distill Requires from rebuilt binary packages and serialize them into file.
# Return true on success.
# Needs `rpms'.
sub storebinaryrequires {
    my $self = shift;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'runrequiresstore');
    if ($lock->is_done) { return 1; }

    my @rpms = glob(File::Spec->catfile($self->rpmdir, '*.rpm'));
    if ($#rpms < 0) {
        $lock->log("No binary RPM packages found in `" . $self->rpmdir
            . "'\n");
        return $lock->mark_failed;
    }

    my $allrequires = {};
    for my $rpmname (@rpms) {
        my $rpm = Fedora::Rebuild::RPM->new(name => $rpmname);

        my ($requires, $envr) = $rpm->requires;

        if (! defined $requires || ! defined $envr) {
            $lock->log("Could not get run-time requires of RPM `" . $rpmname .
                "': " .  $@ ."\n");
            return $lock->mark_failed;
        }
        $$allrequires{$envr} = $requires;
    }

    if (! $lock->nstorereference($allrequires, $self->runrequiresstore)) {
        $lock->log("Could not store run-time requires of RPM `". $self->name .
            "' into `" . $self->runrequiresstore . "' file: $@\n");
        return $lock->mark_failed;
    }

    $lock->log(Data::Dumper::Dumper($allrequires));
    return $lock->mark_done;
}

# Distill Provides from rebuilt binary packages and serialize them into file.
# Return true on success.
# Needs `rpms'.
sub storebinaryprovides {
    my $self = shift;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'providesstore');
    if ($lock->is_done) { return 1; }

    my @rpms = glob(File::Spec->catfile($self->rpmdir, '*.rpm'));
    if ($#rpms < 0) {
        $lock->log("No binary RPM packages found in `" . $self->rpmdir
            . "'\n");
        return $lock->mark_failed;
    }

    my $allprovides = {};
    for my $rpmname (@rpms) {
        my $rpm = Fedora::Rebuild::RPM->new(name => $rpmname);

        my ($provides, $envr) = $rpm->provides;

        if (! defined $provides || !defined $envr) {
            $lock->log("Could not get provides of RPM `" . $rpmname . "': " .
                $@ ."\n");
            return $lock->mark_failed;
        }
        $$allprovides{$envr} = $provides;
    }

    if (! $lock->nstorereference($allprovides, $self->providesstore)) {
        $lock->log("Could not store provides of RPM `". $self->name .
            "' into `" . $self->providesstore . "' file: $@\n");
        return $lock->mark_failed;
    }

    $lock->log(Data::Dumper::Dumper($allprovides));
    return $lock->mark_done;
}

# Load run-time requires of already rebuilt binary packages from file.
# Return true on success.
# Needs `storebinaryrequires'.
sub loadbinaryrequires {
    my $self = shift;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'runrequires');
    if ($lock->is_done) { return 1; }

    my $runrequires = $lock->retrievereference($self->runrequiresstore);
    if (! $runrequires) {
        $lock->log("Could not load run-time requires of `". $self->name .
            "' package from `" . $self->runrequiresstore . "' file: $@\n");
        return $lock->mark_failed;
    }
    $self->runrequires(shared_clone($runrequires));
    
    $lock->log(Data::Dumper::Dumper($self->runrequires));
    return $lock->mark_done;
}

# Load provides of already rebuilt binary packages from file.
# Return true on success.
# Needs `storebinaryprovides'.
sub loadbinaryprovides {
    my $self = shift;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'provides');
    if ($lock->is_done) { return 1; }

    my $provides = $lock->retrievereference($self->providesstore);
    if (! $provides) {
        $lock->log("Could not load provides of `". $self->name .
            "' package from `" . $self->providesstore . "' file: $@\n");
        return $lock->mark_failed;
    }
    $self->provides(shared_clone($provides));
    
    $lock->log(Data::Dumper::Dumper($self->provides));
    return $lock->mark_done;
}

# Edit git repository by an external tool if not yet done. Does not increase
# package revisions.
# First argument is mode. Only in "koji" mode pushes the changes.
# Second argument is an external editor command. The command is expressed as
# an array reference. Its working directory will be the git repository
# directory. Its last argument will be the spec file name. The command can
# abort edition by non-zero exit code.
# Return true on success.
# Needs `clone'.
sub edit {
    my ($self, $mode, $editor) = @_;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'edit');
    if ($lock->is_done) { return 1; }

    if (ref $editor ne 'ARRAY') {
        $lock->log("The editor argument (" . "$editor" .
            ") is not an array reference.\n");
        return $lock->mark_failed;
    }

    if (!@{$editor}) {
        $lock->log("The editor command is an empty list.\n");
        return $lock->mark_failed;
    }

    # Reset repository
    if (!$self->refreshgit($lock)) {
        return $lock->mark_failed;
    }

    # Execute external editor
    if (!$lock->do($self->repodir, @{$editor}, $self->name . '.spec')) {
        $lock->log("Could not edit spec file for `" . $self->name .
            "' package.\n");
        return $lock->mark_failed;
    }

    # Commit and push changes only if editor has changed anything
    my $modified = $self->gitismodified($lock);
    if (-1 == $modified) {
        $lock->log("Could not check status of changes in git repository `" .
            $self->repodir . "'.\n");
        return $lock->mark_failed;
    } elsif ($modified) {
        $lock->log("Editor did changes." .
            " They will be committed and possibly pushed.\n");
        if (!$self->commitandpush($lock, $mode)) {
            return $lock->mark_failed;
        }
    } else {
        $lock->log("Editor did not change anything." .
            " Nothing to commit and push.\n");
    }
    return $lock->mark_done;
}


# Increase package revision if not yet done. Commit change if first argument
# is "koji".
# Return true on success.
# Needs `clone'.
sub update {
    my ($self, $mode) = @_;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'update');
    if ($lock->is_done) { return 1; }

    # Update spec file
    if (!$self->updatespec($lock)) {
        $lock->log("Could not update spec file in order to create new " .
            "update for `" . $self->name . "' package.\n");
        return $lock->mark_failed;
    }

    # Commit and push changes
    if (!$self->commitandpush($lock, $mode)) {
        return $lock->mark_failed;
    }

    return $lock->mark_done;
}


# Submit package for building into Koji and store task ID.
# This is pointless in local build mode.
# First argument is Fedora::Rebuild::Package::Config object.
# Requires `clone'. Sould be called after `update'.
# Return true on success.
sub submitbuild {
    my ($self, $build_config) = @_;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'submitbuild');
    if ($lock->is_done) { return 1; }

    # Get NEVRA of intended build
    my $nevra = $self->get_nevra_from_git($lock, $build_config);
    if (! defined $nevra) {
        return $lock->mark_failed;
    }

    # Check the build is not already in Koji (e.g. by concurrent packager
    # or after this program restart) but do not conclude anything.
    my $buildinfo;
    if (!$lock->dooutput($self->repodir, \$buildinfo, $build_config->koji(),
            'buildinfo', $nevra)) {
        $lock->log("Could not ask Koji for `" . $nevra . "' status " .
            "before submitting new build.\n");
        return $lock->mark_failed;
    }
    if ($buildinfo =~ /No such build/m) {
        $lock->log("Package not yet submitted for building as expected.\n");
    } else {
        # Get task ID of already building package
        if ($buildinfo =~ /Task:\s*(\d+)/m) {
            # TODO: We could compare task target and consider as submitted if
            # equaled to intended target.
            my $task_id = $1;
            $lock->log("Package `$nevra' already submitted as task " .
                "`$task_id'. Previous build failed or somebody builds the " .
                "package concurrently.\n");
        } else {
            $lock->log("Package `$nevra' already in Koji, but task ID " .
                "could not been determined.\n");
        }
        $lock->log("Re-submitting the package.\n")
    }

    # Submit into Koji
    my $task_id;
    if (!$lock->dooutput($self->repodir, \$task_id, $build_config->pyrpkg(),
            (defined $self->pkgdist) ? ('--dist', $self->pkgdist) : (),
            'build', '--nowait', '--target', $self->target)) {
        $lock->log("Could not submit `" . $nevra . "' into Koji.\n");
        return $lock->mark_failed;
    }
    if (not $task_id =~ /Created task:\s*(\d+)/) {
        $lock->log("Could not parse Koji task ID for `$nevra' build\n");
        return $lock->mark_failed;
    }
    $task_id = $1;

    # Store task ID
    if (! $lock->nstorereference(\$task_id, $self->taskstore)) {
        $lock->log("Could not store task ID `" . $task_id . "' of `" . $nevra .
            "' package into `" . $self->taskstore . "' file: $@\n");
        return $lock->mark_failed;
    } else {
        $lock->log("Task ID `" . $task_id . "' stored into `" .
            $self->taskstore . "' file sucessfully.\n");
    }

    return $lock->mark_done;
}


# First argument is Fedora::Rebuild::Package::Config module. If mode member is:
#   "koji"      wait for package build in Koji,
#   "local"     build locally using pyrpkg.
#   "mock"      build locally using mock.
# Second argument is Mock object to use instead of fresh one (value undef).
# (This is safe only in single-threaded run. This is limitation of mock.)
# Requires `clone'. Sould be called after `update' or `submitbuild'.
# Third argument is Mock object to use instead of fresh one (value undef) for
# building missing SRPMs.
# Return true on success.
sub build {
    my ($self, $build_config, $mock, $src_mock) = @_;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'build');
    if ($lock->is_done) { return 1; }

    my $nevra = $self->get_nevra_from_git($lock, $build_config);
    if (! defined $nevra) {
        return $lock->mark_failed;
    }

    if ($build_config->mode() eq 'local') {
        if (!$lock->do($self->repodir, $build_config->pyrpkg(),
                (defined $self->pkgdist) ? ('--dist', $self->pkgdist) : (),
                'local')) {
            $lock->log("Could not build `" . $nevra . "' locally.\n");
            return $lock->mark_failed;
        }
    } elsif ($build_config->mode() eq 'koji') {
        # Retrieve task ID of submitted build
        my $task_id = $lock->retrievereference($self->taskstore);
        if (! $task_id) {
            $lock->log("Could not load task ID for `". $nevra .
                "' build from `" . $self->taskstore . "' file: $@\n");
            return $lock->mark_failed;
        }
        $task_id = $$task_id;
       
        # Wait for build task result
        # TODO: How to recognize the process died for other reason
        # than build failure?
        if (!$lock->do($self->repodir, $build_config->koji(),
                'watch-task', $task_id)) {
            $lock->log("Could not get status of Koji task `" . $task_id .
                "' for `$nevra' build, or the task failed.\n");
            return $lock->mark_failed;
        }
    } elsif ($build_config->mode() eq 'mock') {
        my $srpm_name = $self->get_srpm_name($lock, $build_config, $src_mock);
        if (! defined $srpm_name) {
            return $lock->mark_failed;
        }
        if (!replace_directory($lock, $self->mockdir)) {
            return $lock->mark_failed;
        }
        my ($mock_config_dir, $mock_config_root);
        eval {
            if (!defined $mock) { 
                $mock = Fedora::Rebuild::Mock->new(
                    architecture => $build_config->architecture(),
                    repositories => $build_config->repositories(),
                    install_packages => $build_config->mock_install_packages(),
                );
                $mock->make_configuration;
            }
            $mock_config_dir = $mock->config_dir;
            $mock_config_root = $mock->config_root;
        };
        if ($@) {
            $lock->log("Could not configure mock: $@\n");
            File::Path::Tiny::rm($mock_config_dir);
            return $lock->mark_failed;
        }
        my $success = $lock->do(undef, 'mock', '--resultdir', $self->mockdir,
                '--configdir', $mock_config_dir, '--root', $mock_config_root,
                '--rebuild', $srpm_name);
        if (defined $mock && !$mock->shared) {
            File::Path::Tiny::rm($mock_config_dir);
        }
        if (!$success) {
            $lock->log("Could not build `" . $nevra . "' in mock.\n");
            return $lock->mark_failed;
        }
    } else {
        $lock->log("Could not build `" . $nevra .
            "' because of unknown building mode `" . $build_config->mode() .
            "'.\n");
        return $lock->mark_failed;
    }

    return $lock->mark_done;
}


# Waits for build root rotation. Just built package will be available for
# other packages at build time after returning from this fuction.
# First argument is Fedora::Rebuild::Package::Config object.
# If the mode is "koji", it will wait for the Koji rotation.
# If the mode is "mock", it will create yum repository in directory with
# binary packages.
# If the mode is "local", this function is void.
# Requires `update'. Sould be called after `build'.
# Return true on success.
sub dowaitforbuildroot {
    my ($self, $build_config) = @_;
    my $lock = Fedora::Rebuild::Package::StateLock->new(package => $self,
       state => 'rotate');
    if ($lock->is_done) { return 1; }

    my $nevra = $self->get_nevra_from_git($lock, $build_config);
    if (! defined $nevra) {
        return $lock->mark_failed;
    }

    if ($build_config->mode() eq 'koji') {
        if (!$lock->do($self->repodir, $build_config->koji(), 'wait-repo', 
                '--build=' . $nevra, '--target', $self->target)) {
            $lock->log("Koji does not contain `" . $nevra .
                "' package in build root for `" . $self->target .
                "' build target yet.\n");
            return $lock->mark_failed;
        }
    } elsif ($build_config->mode() eq 'mock') {
        $lock->log("`" . $nevra .
            "' built in mock, creating yum repository...\n");
        if (!$lock->do($self->rpmdir, 'createrepo_c', '.')) {
            $lock->log("Could not create yum repository for `" . $nevra .
                "' package.\n");
            return $lock->mark_failed;
        }
        $lock->log("`" . $nevra .
            "' yum repository created successfully.\n");
    } else {
        $lock->log("`" . $nevra .
            "' built locally, not waiting on Koji rotation.\n");
    }

    return $lock->mark_done;
}


# Reset 'rotate' state by removing directory with binary packages produced by
# the package.
# TODO: Untag package from koji?
# Return true on success, otherwise false.
sub reset_rotate {
    my ($self) = @_;

    my $lock = Fedora::Rebuild::Package::StateLock->new(
        package => $self, state => 'rotate');
    $lock->log("Reseting state...\n");
    if (!remove_directory($lock, $self->rpmdir)) {
        return $lock->mark_failed;
    }
    $lock->mark_failed;
    $lock->remove_log;
    return 1;
}


# Set hash of build-time dependencies (requires attribute).
# First argument is Fedora::Rebuild::Package::Config object. If mode member is:
#   "local"             build SRPM locally using pyrpkg,
#   "koji" or "mock"    build SRPM using mock.
# Second argument is anonymous clone boolean.
# Third argument is Mock object to use instead of fresh one (value undef).
# (This is safe only in single-threaded run. This is limitation of mock.)
# Fourth argument is Mock object to use instead of fresh one (value undef).
# Return undef in case of failure.
sub get_buildrequires {
    my ($self, $build_config, $anonymous, $mock, $src_mock) = @_;
    my $ok;
    print "Getting BuildRequires for `" . $self->name . "'...\n";

    # Procede all steps, each must be re-doable
    $ok = $self->clone($anonymous, $build_config);
    $ok = $self->srpm(0, $build_config, $mock) if $ok;
    $ok = $self->storebuildrequires($build_config, $src_mock) if $ok;
    $ok = $self->buildrequires if $ok;
    
    if ($ok) {
        print "BuildRequires for `" . $self->name .
            "' package distilled successfully.\n";
        return 1;
    } else {
        print "Could not get BuildRequires for `" . $self->name .
            "' package.\n";
        return undef;
    }
}

# Set hash of run-time requires (Requires attribute).
# Return true on success, undef in case of failure.
# XXX: Requires `runrequiresstore'
sub get_binaryrequires {
    my $self = shift;
    my $ok;
    print "Getting run-time requires for `" . $self->name . "'...\n";

    $ok = $self->storebinaryrequires;
    $ok = $self->loadbinaryrequires if $ok;

    if ($ok) {
        print "Run-time requires for `" . $self->name .
            "' package distilled successfully.\n";
        return 1;
    } else {
        print "Could not get run-time requires for `" . $self->name .
            "' package.\n";
        return undef;
    }
}

# Set hash of run-time provides (provides attribute).
# Return true on success, undef in case of failure.
# XXX: Requires `providesstore'
sub get_binaryprovides {
    my $self = shift;
    my $ok;
    print "Getting binary provides for `" . $self->name . "'...\n";

    $ok = $self->storebinaryprovides;
    $ok = $self->loadbinaryprovides if $ok;

    if ($ok) {
        print "Provides for `" . $self->name .
            "' package distilled successfully.\n";
        return 1;
    } else {
        print "Could not get Provides for `" . $self->name . "' package.\n";
        return undef;
    }
}

# Set hash of run-time requires (Requires attribute) and provides
# (provides attribute). Common wrapper for getbinaryrequires() and
# getbinaryprovides().
# Return true on success, undef in case of failure.
# XXX: Requires `runrequiresstore' and `providesstore'
sub get_binarydependencies {
    my $self = shift;
    my $ok;
    print "Getting binary dependencies for `" . $self->name . "'...\n";

    $ok = $self->get_binaryrequires;
    $ok = $self->get_binaryprovides if $ok;

    if ($ok) {
        print "Binary dependencies for `" . $self->name .
            "' package distilled successfully.\n";
        return 1;
    } else {
        print "Could not get binary dependencies for `" . $self->name .
            "' package.\n";
        return undef;
    }
}

# Edit a package. It clones it, edit it and push the changes.
# Unchanged packages are not pushed, but still reported as successfully
# processed.
# First argument is Fedora::Rebuild::Package::Config object. mode member
# defines build mode:
#   "koji"      publish commit,
#   "local"     without pushing commits to server,
# Second argument is anonymous clone boolean.
# Third argument is a program to execute as the editor as an array reference.
sub cloneeditpush {
    my ($self, $build_config, $anonymous, $editor) = @_;
    my $ok;
    print "Editing `" . $self->name . "'...\n";

    # Proceed all steps, each must be re-doable
    $ok = $self->clone($anonymous, $build_config);
    $ok = $self->edit($build_config->mode(), $editor) if $ok;

    if ($ok) {
        print "`" . $self->name . "' edit finished successfully.\n";
    } else {
        print "`" . $self->name . "' edit failed.\n";
    }
    return $ok;
}

# Rebuild a package without waiting for propagation to next build root.
# First argument is Fedora::Rebuild::Package::Config object. mode member
# defines build mode:
#   "koji"      publish commit and build in Koji,
#   "local"     build locally without pushing commits to server,
#   "mock"      build in mock without pushing commits to server.
# Second argument is anonymous clone boolean.
# Third argument is Mock object to use instead of fresh one (value undef).
# (This is safe only in single-threaded run. This is limitation of mock.)
sub rebuild {
    my ($self, $build_config, $anonymous, $mock) = @_;
    my $ok;
    print "Rebuilding `" . $self->name . "'...\n";

    # Proceed all steps, each must be re-doable
    $ok = $self->clone($anonymous, $build_config);
    $ok = $self->update($build_config->mode()) if $ok;
    $ok = $self->submitbuild($build_config)
        if ($ok and $build_config->mode() eq 'koji');
    $ok = $self->build($build_config, $mock) if $ok;
    $ok = $self->binaryrpm($build_config) if $ok;
    $ok = $self->get_binaryprovides if $ok;
    $ok = $self->get_binaryrequires if $ok;

    if ($ok) {
        print "`" . $self->name . "' rebuild finished successfully.\n";
    } else {
        print "`" . $self->name . "' rebuild failed.\n";
    }
    return $ok;
}

# Wait for the package propagated into new build root.
# First argument is Fedora::Rebuild::Package::Config object.
# If the mode is "koji", waits for Koji build root rotation,
# Otherwise waits locally (no-op).
# XXX: Requires `update', should be called after rebuilding package.
sub waitforbuildroot {
    my ($self, $build_config) = @_;
    my $ok;
    print "Waiting for `" . $self->name . "' to get to build root...\n";

    $ok = $self->dowaitforbuildroot($build_config);

    if ($ok) {
        print "`" . $self->name . "' propagated successfully.\n";
    } else {
        print "`" . $self->name . "' propagation failed.\n";
    }
    return $ok;
}

1;
