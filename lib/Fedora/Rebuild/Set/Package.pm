package Fedora::Rebuild::Set::Package;
use strict;
use warnings;
use version 0.77; our $VERSION = version->declare("v0.12.1");

use Moose;
use Scalar::Util qw( blessed );
use namespace::clean;


has 'set' => (
    is => 'ro',
    isa => 'HashRef[Fedora::Rebuild::Package]',
    default   => sub { {} },
);

# Usage: $set->insert(F::R::Package)
sub insert {
    my ($self, $package) = @_;
    ${$self->set}{$package->name} = $package;
    return $self;
}

# Accepts F::R::Package or name string
sub contains {
    my ($self, $name) = @_;
    if (!(defined $name)) { return 0; }
    if (blessed $name) {
        $name = $name->name;
    }
    return exists ${$self->set}{$name};
}

# Return number of packages in the set
sub size {
    my $self = shift;
    return scalar(keys %{$self->set});
}

# Return list of package names in the set
sub names {
    my $self = shift;
    return keys %{$self->set};
}

# Return list of packages in the set
sub packages {
    my $self = shift;
    return values %{$self->set};
}

# Stringify sorted list of package names as one string (separate by separator)
# Usage: $set->string(' ');
# or:    $set->string;  # Default separotor is a space
sub string {
    my ($self, $separator) = @_;
    if (!defined $separator) { $separator = ' '; }
    return join($separator, sort($self->names));
}

# Return a package by F::R::Package or name string
# Return undef if it does not exist.
sub get {
    my ($self, $name) = @_;
    if (!(defined $name)) { return 0; }
    if (blessed $name) {
        $name = $name->name;
    }
    if (exists ${$self->set}{$name}) {
        return ${$self->set}{$name}; }
    else {
        return undef;
    }
}


# Remove a package by F::R::Package or name string
sub delete {
    my ($self, $name) = @_;
    if (!(defined $name)) { return 0; }
    if (blessed $name) {
        $name = $name->name;
    }
    if (exists ${$self->set}{$name}) {
        delete ${$self->set}{$name}; 
    }
    return $self;
}


1;
