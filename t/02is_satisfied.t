#!/usr/bin/perl
use strict;
use warnings;

use Test::Simple tests => 4 + 3*5*5 + 3;

use Fedora::Rebuild::RPM;

# The ok() labels have format "REQUIRES ? COMMA_SEPARATED_PROVIDES".

# Non-flag-qualified cases
ok(
    Fedora::Rebuild::RPM::is_satisfied(
        'a', Fedora::Rebuild::RPM::ANY, '',
        {
            'a' => [ [Fedora::Rebuild::RPM::ANY, ''] ]
        }
    ), 'a ? a'
);

ok(
    ! Fedora::Rebuild::RPM::is_satisfied(
        'a', Fedora::Rebuild::RPM::ANY, '',
        {
            'b' => [ [Fedora::Rebuild::RPM::ANY, ''] ]
        }
    ), 'a ? b'
);

ok(
    Fedora::Rebuild::RPM::is_satisfied(
        'a', Fedora::Rebuild::RPM::ANY, '',
        {
            'b' => [ [Fedora::Rebuild::RPM::ANY, ''] ],
            'a' => [ [Fedora::Rebuild::RPM::ANY, ''] ],
            'c' => [ [Fedora::Rebuild::RPM::ANY, ''] ]
        }
    ), 'a ? b, a, c'
);

ok(
    ! Fedora::Rebuild::RPM::is_satisfied(
        'a', Fedora::Rebuild::RPM::ANY, '',
        {
        }
    ), 'a ? '
);


# A matrix case. See module comments for the matrices. There should be 3*5*5
# cases in total.
sub test_matrix_cell {
    my ($order, $rflag, $pflag, $result) = @_;

    my ($rversion, $pversion) = ('1', '1');
    if ($order < 0) {
        $rversion = '0';
    } elsif ($order > 0) {
        $rversion = '2';
    }

    ok(
        $result == Fedora::Rebuild::RPM::is_satisfied(
            'a', $rflag, $rversion,
            {
                'a' => [ [$pflag, $pversion] ]
            }
        ),
        'a ' . Fedora::Rebuild::RPM::flag_as_string($rflag) . " $rversion" .
            ' ? a ' . Fedora::Rebuild::RPM::flag_as_string($pflag) .
            " $pversion"
    );
}

# Test whole matrix of flags (5*5) for given version order
sub test_matrix {
    my ($order, @results) = @_;

    my @flags = (
        Fedora::Rebuild::RPM::LESS,
        Fedora::Rebuild::RPM::LESS|Fedora::Rebuild::RPM::EQUAL,
        Fedora::Rebuild::RPM::EQUAL,
        Fedora::Rebuild::RPM::GREATER|Fedora::Rebuild::RPM::EQUAL,
        Fedora::Rebuild::RPM::GREATER
    );

    my $i = 0;
    for my $rflag (@flags) {
        for my $pflag (@flags) {
            test_matrix_cell($order, $rflag, $pflag, $results[$i++]);
        }
    }
}


test_matrix(
    # (labelCompare($rversion,$pversion) = $order) == 0:
        0,
    #           pflag
    #   <   <=  ==  >=  >
        1,  1,  0,  0,  0,  # rflag <   
        1,  1,  1,  1,  0,  #       <=  
        0,  1,  1,  1,  0,  #       ==  
        0,  1,  1,  1,  1,  #       =>  
        0,  0,  0,  1,  1   #       >   
);

test_matrix(
    # (labelCompare($rversion,$pversion) = $order) > 0:
        1,
    #           pflag
    #   <   <=  ==  >=  >
        1,  1,  1,  1,  1,  # rflag <   
        1,  1,  1,  1,  1,  #       <=  
        0,  0,  0,  1,  1,  #       ==  
        0,  0,  0,  1,  1,  #       =>  
        0,  0,  0,  1,  1   #       >   
);

test_matrix(
    # (labelCompare($rversion,$pversion) = $order) < 0:
        -1,
    #           pflag
    #           <   <=  ==  >=  >
        1,  1,  0,  0,  0,  # rflag <    
        1,  1,  0,  0,  0,  #       <=  
        1,  1,  0,  0,  0,  #       ==  
        1,  1,  1,  1,  1,  #       =>  
        1,  1,  1,  1,  1   #       >   
);


# Some generic tests
ok(
    Fedora::Rebuild::RPM::is_satisfied(
        'a', Fedora::Rebuild::RPM::GREATER|Fedora::Rebuild::RPM::EQUAL, '1',
        {
            'a' => [ [Fedora::Rebuild::RPM::ANY, ''] ]
        }
    ), 'a >= 1 ? a'
);

ok(
    Fedora::Rebuild::RPM::is_satisfied(
        'a', Fedora::Rebuild::RPM::GREATER|Fedora::Rebuild::RPM::EQUAL, '1',
        {
            'a' => [ [Fedora::Rebuild::RPM::EQUAL, '2'] ]
        }
    ), 'a >= 1 ? a = 2'
);

ok(
    ! Fedora::Rebuild::RPM::is_satisfied(
        'a', Fedora::Rebuild::RPM::GREATER|Fedora::Rebuild::RPM::EQUAL, '1',
        {
            'a' => [ [Fedora::Rebuild::RPM::EQUAL, '0'] ]
        }
    ), 'a >= 1 ? a = 0'
);

