#!/usr/bin/perl
use strict;
use warnings;

use Test::Simple tests => 5*5;

use Fedora::Rebuild::Solver;
use Fedora::Rebuild::Set::Package;
use Fedora::Rebuild::Package;
use Fedora::Rebuild::RPM;
use threads::shared;

# Prepare packages
# Create and return package with one binary package that always provides its
# unversioned name.
sub build_package {
    my ($name, $provides, $runrequires) = @_;

    my $package = Fedora::Rebuild::Package->new(
        'name' => $name,
        workdir => 't/tmp',
        dist => 'foo',
        target => 'foo',
        message => 'foo',
    );

    $package->provides(shared_clone(
            {
                $name => {
                    $name => [[Fedora::Rebuild::RPM::ANY, '']],
                    %$provides
                }
            }
        ));
    $package->runrequires(shared_clone(
            {
                $name => $runrequires
            }
        ));
    return $package;
}

my $a = build_package(
    'a',
    {
        'foo' =>
            [[Fedora::Rebuild::RPM::EQUAL|Fedora::Rebuild::RPM::GREATER, '1']],
    },
    {}
);

my $b = build_package(
    'b',
    {},
    { 'foo' => [[Fedora::Rebuild::RPM::ANY, '']] }
);

my $c = build_package(
    'c',
    {},
    { 'b' => [[Fedora::Rebuild::RPM::ANY, '']] }
);

my $d = build_package(
    'd',
    {},
    { 'foo' => [[Fedora::Rebuild::RPM::EQUAL, '0']] }
);

my $e = build_package(
    'e',
    {},
    {
        'foo' => [[Fedora::Rebuild::RPM::EQUAL, '0']],
        'bar' => [[Fedora::Rebuild::RPM::ANY, '']]
    }
);


# Prepare set of packages
my $packages = Fedora::Rebuild::Set::Package->new();
$packages->insert($a);
$packages->insert($b);
$packages->insert($c);
$packages->insert($d);
$packages->insert($e);

# Test group of packages
sub do_test {
    my ($filter_name, $filter_sub, @excpected_results) = @_;
    my $message;

    my $solver = Fedora::Rebuild::Solver->new(packages => $packages,
        dependencyfilter => $filter_sub);

    ok($solver->is_installable($a, 'a', \$message) == shift @excpected_results,
        "$filter_name, a does require nothing: $message");
    ok($solver->is_installable($b, 'b', \$message) == shift @excpected_results,
        "$filter_name, b requires foo provided by a: $message");
    ok($solver->is_installable($c, 'c', \$message) == shift @excpected_results,
        "$filter_name, c requires b that requires foo provided by a: $message");
    ok($solver->is_installable($d, 'd', \$message) == shift @excpected_results,
        "$filter_name, d requires non-existing foo = 0: $message");
    ok($solver->is_installable($e, 'e', \$message) == shift @excpected_results,
        "$filter_name, e requires non-existing foo = 0 and bar: $message");
}

# Test
do_test('Explicit 0 filter', sub {0}, 1, 1, 1, 0, 0);
do_test('Explicit 1 filter', sub {1}, 1, 1, 1, 1, 1);

do_test('Filter out any foo',
    sub {
        my ($name, $flag, $version) = @_;
        $name eq 'foo';
    }, 1, 1, 1, 1, 0);

do_test('Filter out foo = 0',
    sub {
        my ($name, $flag, $version) = @_;
        $name eq 'foo' and $flag == Fedora::Rebuild::RPM::EQUAL and
            $version eq '0';
    }, 1, 1, 1, 1, 0);

do_test('Filter out any bar',
    sub {
        my ($name, $flag, $version) = @_;
        $name eq 'bar' and $flag;
    }, 1, 1, 1, 0, 0);

