package Fedora::Rebuild::Providers;
use strict;
use warnings;
use version 0.77; our $VERSION = version->declare("v0.9.1");

use Moose;
use Fedora::Rebuild::Package;
use Fedora::Rebuild::RPM;
use namespace::clean;

=encoding utf8

=head1 NAME

Fedora::Rebuild::Providers - Reverse cache for RPM provides

=head1 SYNOPSIS

    # Prepare a package
    use threads::shared;
    use Fedora::Rebuild::Package;
    my $package = Fedora::Rebuild::Package->new(...);
    $package->provides(shared_clone(...));

    # Create a cache
    use Fedora::Rebuild::Providers;
    my $providers = Fedora::Rebuild::Providers->new();

    # Insert the package into the cache
    $providers->insert($package);

    # Query the cache for package providing 'x' of any version
    use Fedora::Rebuild::RPM;
    my @found = $providers->what_provides(
        'x', Fedora::Rebuild::RPM::ANY, ''
    );

=head1 DESCRIPTION

This is reverse cache for RPM package provides. You can instert many packages
with populated provides and then you can ask which packages provide given RPM
symbol.

Objects of this class are not threads-shared.

=head1 Constructor

=head2 new

There are no constructor arguments. New object has empty I<providers>.

=cut

=head1 Read only accessors

=head2 providers

This is a mapping from RPM provides to binary packages providing them.
This is a reference to hash of binary package provides names.
Hash format is:

    { NAME1 => [
          [FLAG1, VERSION1, SOURCENAME1, BINARYRPM1],
          [FLAG1, VERSION1, SOURCENAME1, BINARYRPM2],
          [FLAG1, VERSION1, SOURCENAME2, BINARYRPM2],
          [FLAG2, VERSION2, SOURCENAME1, BINARYRPM1],
      ],
      NAME2 => [
          ...
      ],
      ...
    }

Be ware the same provided symbol can be provided by more packages.

=cut

has 'providers' => (is => 'rw', isa => 'HashRef', lazy_build => 1,
    init_arg => 0);

sub _build_providers {
    return {};
}

=head1 Methods

=head2 insert($package)

Copy all providers of a $package into the object. $package is
L<Fedora::Rebuild::Package> object.

=cut

sub insert {
    my ($self, $package) = @_;
    my $providers = $self->providers();

    # See Fedora::Rebuild::RPM::provides() for $package->provides format.
    # XXX: Do not use `each' function. It would loop. See
    # Moose::Meta::Attribute::Native::Trait::Hash documentation:
    # Note that each is deliberately omitted, due to its stateful interaction
    # with the hash iterator. keys or kv are much safer.
    for my $binary_package (keys %{$package->provides}) {
        my $binary_package_provides = ${$package->provides}{$binary_package};
        for my $pname (keys %{$binary_package_provides}) {
            my $qualifiers = ${$binary_package_provides}{$pname};
            for my $qualifier (@{$qualifiers}) {
                push @{$$providers{$pname}},
                    [
                        @{$qualifier},
                        $package,
                        $binary_package
                    ];
            }
        }
    }

    1;
}


=head2 what_provides($name, $flag, $version)

Query the object which packages provide given ($name, $flag, $version)
triplet.

The $name is a string, the $flag is binary field of L<Fedora::Rebuild::RPM>
version qualifiers (e.g.
C<Fedora::Rebuild::RPM::EQUAL|Fedora::Rebuild::RPM::GREATER>), the $version is
a string.

The return value is an array of packages which satisfy the symbol. The array
is list of refences to pairs:

    (
        [ SOURCE_PACKAGE1, BINARY_PACKAGE1 ],
        [ SOURCE_PACKAGE2, BINARY_PACKAGE2 ],
        ...
    )

Second element is the binary package name, first element is
source package of the binary package expressed as
a L<Fedora::Rebuild::Package> object.

=cut

sub what_provides {
    my ($self, $name, $flag, $version) = @_;

    my $providers = $self->providers();

    if (!exists ${$providers}{$name}) {
        return ();
    }

    my @packages = ();
    for my $provide (@{$$providers{$name}}) {
        if (Fedora::Rebuild::RPM::version_is_satisfied($flag, $version,
                @{$provide}[0,1])) {
            push @packages, [@{$provide}[2,3]];
        }
    }

    return @packages;
}

1;
