package Fedora::Rebuild::Package::Config;
use strict;
use warnings;
use version 0.77; our $VERSION = version->declare("v0.12.1");

use Moose;
use Fedora::Rebuild::Types qw( Mode );
use namespace::clean;

=encoding utf8

=head1 Fedora::Rebuild::Package::Config

Configuration for building a package. This object and its members are not
shared.

=head2 Constructor arguments and read only accessors

=head3 mode

A C<Mode> type.

Pass C<local> for committing and building locally only,
pass C<mock> for committing localy and building in mock,
pass C<koji> for pushing commits and building in Koji.

=cut

has 'mode' => ( is => 'ro', isa => Mode, required => 1);

=head3 pyrpkg

A string with a program name used for handling package dist-git repositories.
The program must implement I<pyrpkg> front-end, like I<fedpkg> does.

=cut

has 'pyrpkg' => ( is => 'ro', isa => 'Str', required => 1);

=head3 koji

A string with a program name used for building in Koji-like build service.
The program must implement I<koji> command interface.

=cut

has 'koji' => ( is => 'ro', isa => 'Str', required => 1);

=head3 repositories

Reference to array of repository URLs. Usually points to Fedora mirror or
Koji repository (baseurl in YUM terminology). This is needed for C<mock>
build C<mode>.

=cut

has 'repositories' => ( is => 'ro', isa => 'ArrayRef[Str]', required => 1);

=head3 architecture

RPM architecture name, e.g. C<x86_64>.

=cut

has 'architecture' => ( is => 'ro', isa => 'Str', required => 1);

=head3 mock_install_packages

YUM packages to install at mock chroot initialization. Separate them by
a space.  C<@buildsys-build> is needed for mirrored Fedora repositories
C<@build> is needed for direct koji repositories.

=cut

has 'mock_install_packages' => ( is => 'ro', isa => 'Str', required => 1);

1;
