#!/usr/bin/perl
use strict;
use warnings;

use Test::More tests => 1 + 2 * 2 + 5;

use Fedora::Rebuild::Package;
use Fedora::Rebuild::RPM;
use Fedora::Rebuild::Providers;
use threads::shared;

sub build_package {
    my ($name, $provides) = @_;

    my $package = Fedora::Rebuild::Package->new(
        'name' => $name,
        workdir => 't/tmp',
        dist => 'foo',
        target => 'foo',
        message => 'foo',
    );

    $package->provides(shared_clone($provides));

    return $package;
}


my $providers = Fedora::Rebuild::Providers->new();
ok(defined $providers, 'create Fedora::Rebuild::Providers object');

my $a = build_package(
    'a',
    {
        'a-main' => {
            'a' => [
                [Fedora::Rebuild::RPM::ANY, '']
            ],
            'dual' => [
                [Fedora::Rebuild::RPM::EQUAL, '1']
            ],
        },
        'a-libs' => {
            'a.so' => [
                [Fedora::Rebuild::RPM::ANY, '']
            ],
        },
    },
);
ok(defined $a, 'create package a');
$providers->insert($a);
is_deeply(
    $providers->providers(),
    {
        'a' => [
            [Fedora::Rebuild::RPM::ANY, '', $a, 'a-main']
        ],
        'a.so' => [
                [Fedora::Rebuild::RPM::ANY, '', $a, 'a-libs']
        ],
        'dual' => [
            [Fedora::Rebuild::RPM::EQUAL, '1', $a, 'a-main']
        ]
    },
    'insert a'
);

my $b = build_package(
    'b',
    {
        'b-main' => {
            'b' => [
                [Fedora::Rebuild::RPM::ANY, '']
            ],
            'dual' => [
                [Fedora::Rebuild::RPM::EQUAL, '2']
            ],
        },
        'b-libs' => {
            'b.so' => [
                [Fedora::Rebuild::RPM::ANY, '']
            ],
        },
    },
);
ok(defined $b, 'create package b');
$providers->insert($b);
is_deeply(
    $providers->providers(),
    {
        'a' => [
            [Fedora::Rebuild::RPM::ANY, '', $a, 'a-main']
        ],
        'a.so' => [
                [Fedora::Rebuild::RPM::ANY, '', $a, 'a-libs']
        ],
        'b' => [
            [Fedora::Rebuild::RPM::ANY, '', $b, 'b-main']
        ],
        'b.so' => [
                [Fedora::Rebuild::RPM::ANY, '', $b, 'b-libs']
        ],
        'dual' => [
            [Fedora::Rebuild::RPM::EQUAL, '1', $a, 'a-main'],
            [Fedora::Rebuild::RPM::EQUAL, '2', $b, 'b-main']
        ]
    },
    'insert b'
);

my @found = $providers->what_provides('x', Fedora::Rebuild::RPM::ANY, '');
is_deeply(
    \@found,
    [],
    'what provides (x *)'
);

@found = $providers->what_provides('a', Fedora::Rebuild::RPM::ANY, '');
is_deeply(
    \@found,
    [
        [ $a, 'a-main' ]
    ],
    'what provides (a *)'
);

@found = $providers->what_provides('dual',
    Fedora::Rebuild::RPM::EQUAL|Fedora::Rebuild::RPM::GREATER, '3');
is_deeply(
    \@found,
    [],
    'what provides (dual >= 3)'
);

@found = $providers->what_provides('dual',
    Fedora::Rebuild::RPM::EQUAL|Fedora::Rebuild::RPM::GREATER, '2');
is_deeply(
    \@found,
    [
        [ $b, 'b-main' ]
    ],
    'what provides (dual >= 2)'
);

@found = $providers->what_provides('dual',
    Fedora::Rebuild::RPM::EQUAL|Fedora::Rebuild::RPM::GREATER, '1');
is_deeply(
    \@found,
    [
        [ $a, 'a-main' ],
        [ $b, 'b-main' ]
    ],
    'what provides (dual >= 1)'
);
