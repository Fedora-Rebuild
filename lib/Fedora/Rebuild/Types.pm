package Fedora::Rebuild::Types;
use strict;
use warnings;

use MooseX::Types -declare => [
    'Mode'
];
use MooseX::Types::Moose qw( Str );

use namespace::clean;
use version 0.77; our $VERSION = version->declare("v0.12.1");

subtype Mode,
    as Str,
    where { $_ eq 'local' or $_ eq 'koji' or $_ eq 'mock'},
    message { qq{String must be one of: local, koji, mock} };
