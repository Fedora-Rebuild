package Fedora::Rebuild::Package::ListLog;
use strict;
use warnings;
use version 0.77; our $VERSION = version->declare("v0.12.1");

use Moose;
use Carp;
use IO::Handle;
use Fcntl qw(:flock);
use namespace::clean;

=encoding utf8

=head1

Fedora::Rebuild::Package::ListLog - Append a package name to a file

=head1 SYNOPSIS

    # Prepare a package
    use threads::shared;
    use Fedora::Rebuild::Package;
    my $package = Fedora::Rebuild::Package->new(...);
    $package->provides(shared_clone(...));

    {
        # Create a log file
        my $log = Fedora::Rebuild::Package::ListLog->new(file => 'file');

        # Log the package into a file
        $log->log($package);
    }
    # Here the log file has been closed

=head1 DESCRIPTION

The class allows to append a L<Fedora::Rebuild::Package> name into a file in
a safe way.

=head1 METHODS

=head2 new(file => $file_name)

This class method appends the $package name into the $file_name. Mandatory
attribute is I<log>, a file where to log the package names to.

=cut

has file => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

# Private log file descriptor
has logfd => (
    is => 'ro',
    isa => 'FileHandle',
    lazy_build => 1,
    init_arg => undef
);

sub BUILD {
    my $self = shift;

    if (! defined $self->file || $self->file eq '') {
        croak "Invalid `file' attributed passed to ListLog constructor";
    }
}

sub _build_logfd {
    my $self = shift;
    my $file = IO::Handle->new();
    open($file, '>>', $self->file) or
        croak "Could not open `" . $self->file . "' for appending: $!";
    return $file;
}

=head2 log($package)

Append L<Fedora::Rebuild::Package> package name into the log file. This
operation is atomic.

=cut

sub log {
    my ($self, $package) = @_;
   
    flock($self->logfd, LOCK_EX) or
        croak "Could not lock `" . $self->file . "': $!";

    $self->logfd->print($package->name . "\n") or
        croak "Could not write data into `" . $self->file . "': $!";
    $self->logfd->flush && $self->logfd->sync or
        croak "Could not flush `" . $self->file . "': $!";

    flock($self->logfd, LOCK_UN) or
        croak "Could not unlock `" . $self->file . "': $!";

}

sub DEMOLISH {
    my $self = shift;
    close($self->logfd) or
        croak "Could not close `" . $self->file . "': $!";
}
1;
