package Fedora::Rebuild::Package::StateLock;
use strict;
use warnings;
use version 0.77; our $VERSION = version->declare("v0.12.1");

use Moose;
use Carp;
use Proc::SyncExec;
use POSIX;
use Data::Dumper;
use Storable qw(nstore_fd retrieve);
use DateTime;
use namespace::clean;
use IO::Handle;

has package => (
    is => 'ro',
    isa => 'Fedora::Rebuild::Package',
    required => 1
);

has state => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

has lockfile => (
    is => 'ro',
    isa => 'Str',
    lazy_build => 1,
    init_arg => undef
);

has logfile => (
    is => 'ro',
    isa => 'Str',
    lazy_build => 1,
    init_arg => undef
);

has logfd => (
    is => 'ro',
    isa => 'FileHandle',
    lazy_build => 1,
    init_arg => undef
);
sub BUILD {
    my $self = shift;

    if (! defined $self->state || $self->state eq '') {
        croak "Invalid `state' attributed passed to StateLock constructor";
    }
}

sub _build_lockfile {
    my $self = shift;
    return $self->package->packagedir . '/.' . $self->state;
}

sub _build_logfile {
    my $self = shift;
    return $self->lockfile. '.log';
}

sub _build_logfd {
    my $self = shift;
    my $file = IO::Handle->new();
    open ($file, '>', $self->logfile) or
        croak "Could not create `" . $self->logfile . "' logfile: $!";
    return $file;
}

# Print array into log
sub lograw {
    shift->logfd->print(@_);
}

# Print current time and array into log
sub log {
    shift->lograw(DateTime->now, ' ', @_);
}

# Return true if state is finshed, otherwise open log file.
sub is_done {
    my $self = shift;

    if (-e $self->lockfile) {
        return 1;
    } else {
        $self->logfd;
        return 0;
    }
}

# Remove lock file (if exists) without any other actions.
# Use mark_failed() to close log file too.
# Return value is not specified.
sub remove_lock {
    my $self = shift;
    if (-e $self->lockfile) { unlink $self->lockfile; }
}

# Fsync and close log file. Croaks on error.
sub close_log {
    my $self=shift;
    $self->logfd->sync && $self->logfd->close or
        croak "Could not sync and close `" . $self->logfile . "' logfile: $!";
}

# Remove log file (if exists) without any other actions.
# Return value is not specified.
sub remove_log {
    my $self = shift;
    if (-e $self->logfile) { unlink $self->logfile; }
}

# Create lock file signalling the state has been finished. is_done() return
# true then. Return true if succeeded.
sub mark_done {
    my $self = shift;
    $self->close_log;
    my $file = IO::Handle->new();
    open ($file, '>', $self->lockfile) or
        croak "Could not open `" . $self->lockfile .
            "' lockfile for writing: $!";
    $file->sync && close($file) or
        croak "Could not sync and close `" . $self->lockfile .
            "' lockfile: $!";
    return 1;
}

# Close log file. Remove lock file if exist. is_done() return false then.
# Return false.
sub mark_failed {
    my $self = shift;
    $self->close_log;
    $self->remove_lock;
    return 0;
}

# Convert ${^CHILD_ERROR_NATIVE} to string description.
# XXX: This is not a method.
sub child_error_as_string {
    my $reason = ${^CHILD_ERROR_NATIVE};
    if (WIFEXITED($reason)) {
        $reason = "exit code " . WEXITSTATUS($reason);
    } elsif (WIFSIGNALED($reason)) {
        $reason = "signal " . WTERMSIG($reason);
    }
    return $reason;
}

# Format array of command with argument as quoted string
# XXX: This not a method
sub format_command {
    $Data::Dumper::Indent=0;
    $Data::Dumper::Terse=1;
    return '(' . join(' ', map {Dumper($_)} @_) . ')';
}

# Run command while appending output to log. Blocks. If workdir is nonempty
# string, switch into it befere execution (and opening the log).
# Return true if command succeeds.
sub do {
    my ($self, $workdir, @command) = @_;

    my $redirect = sub {
        open(STDOUT, '>&', $self->logfd->fileno) and
        open(STDERR, '>&STDOUT');
        $self->log("Executing: " . format_command(@command) . "\n");
        if (defined $workdir && $workdir ne '' && !chdir $workdir) {
            $self->log("Could not change directory to $workdir: $!\n");
            return 0;
        }
        return 1;
    };
    my $pid = Proc::SyncExec::sync_exec($redirect, @command);
    if (!defined $pid) {
        $self->log("Could not execute " . format_command(@command) .  ": $!\n");
        return 0;
    }
    if ($pid != waitpid($pid, 0) || $?) {
        $self->log("Command " . format_command(@command) . " failed: " .
            child_error_as_string . "\n");
        return 0;
    }
    $self->log("Command " . format_command(@command) .
        " returned successfully.\n");
    return 1;
}

# Run command while appending stderr and stdout to log and stdout to refered
# output argument. In case of empty command output fill empty string;
# Blocks. If workdir is nonempty string, switch into it befere execution
# (and opening the log).
# Return true if command succeeds.
sub dooutput {
    my ($self, $workdir, $output, @command) = @_;

    my ($parent, $child);
    if (!pipe $child, $parent) {
        $self->log("Could not get connected pipes for command " .
            format_command(@command) . ": $!\n");
        return 0;
    }

    my $redirect = sub {
        close $child and
        open(STDOUT, '>&', fileno $parent) and
        close $parent and

        open(STDERR, '>&', $self->logfd->fileno) and
        $self->log("Executing: " . format_command(@command) . "\n");
        if (defined $workdir && $workdir ne '' && !chdir $workdir) {
            $self->log("Could not change directory to $workdir: $!\n");
            return 0;
        }
        return 1;
    };
    my $pid = Proc::SyncExec::sync_exec($redirect, @command);
    {
        my $errno = $!;
        close $parent;
        $! = $errno;
    }
    if (!defined $pid) {
        $self->log("Could not execute " . format_command(@command) . ": $!\n");
        return 0;
    }

    for ($$output = ''; local $_ = <$child>;) {
        $$output .= $_;
        $self->lograw($_);
    }

    if ($pid != waitpid($pid, 0) || $?) {
        $self->log("Command " . format_command(@command) . " failed: " .
            child_error_as_string . "\n");
        return 0;
    }

    $self->log("Command " . format_command(@command) .
        " returned successfully.\n");
    return 1;
}

# Serialize referenced variable into file identified by name.
# The file is recreated.
# Return true if command succeeds, otherwise false;
sub nstorereference {
    my ($self, $reference, $filename) = @_;

    unlink($filename);
    my $file = IO::Handle->new();
    if (! open($file, '>', $filename)) {
        $self->log("Could not open `" . $filename . "' file for writing: $!\n");
        return 0;
    }
    if (! eval { nstore_fd($reference, $file); }) {
        $self->log("Could not store variable into `" . $filename .
            "' file: $@\n");
        close($file);
        return 0;
    }
    if (!$file->sync or !close($file)) {
        $self->log("Could not sync and close `" . $filename . "' file: $!\n");
        return 0;
    }

    return 1;
}

# Load serialized variable from file identified by name.
# Return reference to the variable or undef in case of error.
sub retrievereference {
    my ($self, $filename) = @_;

    my $reference;
    if (! eval { $reference = retrieve($filename); } || !$reference) {
        $self->log("Could not load variable from `" . $filename .
            "' file: $@\n");
        return undef;
    }

    return $reference;
}
1;
