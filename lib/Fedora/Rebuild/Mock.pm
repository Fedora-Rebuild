package Fedora::Rebuild::Mock;
use strict;
use warnings;
use version 0.77; our $VERSION = version->declare("v0.12.1");

use Moose;
use Carp;
use File::Spec;
use File::Temp;
use URI;
use namespace::clean;

# Archicture of mock environment. E.g. "x86_64".
has architecture => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

# Reference to list of repository URLs.
has repositories => (
    is => 'ro',
    isa => 'ArrayRef[Str]',
    required => 1
);

# Create a mock environment with enabled caching.
# Default is false.
has shared => (
    is => 'ro',
    isa => 'Bool',
    required => 0,
    default => 0
);

# YUM packages to install at mock chroot initialization. Separate them by
# a space.
# '@buildsys-build' is needed for mirrored Fedora repositories
# '@build' is needed for direct koji repositories.
# Default value is '@buildsys-build'.
has install_packages => (
    is => 'ro',
    isa => 'Str',
    default => '@buildsys-build'
);

# Directory with mock configuration.
has config_dir => (
    is => 'ro',
    isa => 'Str',
    lazy_build => 1,
    init_arg => undef
);

# Base name of configuration file without extension.
has config_root => (
    is => 'ro',
    isa => 'Str',
    default => 'config'
);

sub BUILD {
    my $self = shift;

    if (!defined $self->architecture || $self->architecture eq '') {
        corak("architecture attribute must be non-empty string");
    }
    if ($self->architecture =~ /['"\n]/s) {
        croak("architecture attribute cannot contain " .
            "apostrophe or a double quote or new line");
    }
    if ($self->install_packages =~ /['"\n]/s) {
        croak("install_packages attribute cannot contain " .
            "apostrophe or a double quote or new line");
    }


    for my $repo (@{$self->repositories}) {
        if ($repo =~ /['"\s]/s) {
            croak("repository cannot contain apostrophe or a double quote "
                . "or a white space");
        }
    }
}


# Create a directory. Caller is responsible for removing it.
# Croaks on error.
sub _build_config_dir {
    my $directory;
    local $! = undef;
    eval { $directory = File::Temp::tempdir(CLEANUP => 0); } or
        croak ("Could not create temporary mock configuration directory: " .
            "$!\n");
    return $directory;
}


# Write mock configuration file into the config directory. Returns the full
# pathname to the file.
# Croeaks on error.
sub config_file {
    my $self=shift;
    my $arch = $self->architecture;
    my $install_packages = $self->install_packages;
    # Embed the config_dir random name into mock chroot to allow parallel run
    # of more mock instances. Otherwise mock returns failure on existing lock.
    my (undef, undef, $dir) = File::Spec->splitpath($self->config_dir);

    my $file_name = File::Spec->catfile($self->config_dir,
        ($self->config_root . '.cfg'));
    my $fh;
    local $! = undef;
    open($fh, '>', $file_name) or
        croak("Could not create mock configuration file: $!\n");
    $! = undef;

    binmode($fh, ':utf8');
    if ($self->shared) {
    print $fh <<"FILE";
config_opts['plugin_conf']['ccache_enable'] = True
config_opts['plugin_conf']['yum_cache_enable'] = True
config_opts['plugin_conf']['root_cache_enable'] = True
FILE
    } else {
    print $fh <<"FILE";
config_opts['plugin_conf']['ccache_enable'] = False
config_opts['plugin_conf']['yum_cache_enable'] = False
config_opts['plugin_conf']['root_cache_enable'] = False
FILE
    }
    print $fh <<"FILE";
config_opts['root'] = 'fedorarebuild-$arch-$dir'
config_opts['target_arch'] = '$arch'
config_opts['legal_host_arches'] = ('$arch',)
config_opts['chroot_setup_cmd'] = 'install $install_packages'

config_opts['yum.conf'] = """
[main]
cachedir=/var/cache/yum
debuglevel=1
reposdir=/dev/null
logfile=/var/log/yum.log
retries=20
obsoletes=1
gpgcheck=0
assumeyes=1
syslog_ident=mock
syslog_device=

FILE

    my $id = 0;
    for my $repo (@{$self->repositories}) {
        my $url = URI->new($repo);
        print $fh "[$id]\n";
        print $fh "name=$id\n";
        print $fh "baseurl=$url\n";
        $id++;
    }

    print $fh qq{"""\n};
    $fh->close;
    if ($!) {
        croak("Could not write mock configuration file `" . $file_name .
            "': $!\n");
    }
    return $file_name;
}


# Links configuration file from global configuration directory to this
# specific directory.
# Croaks on error.
sub inherit_file {
    my ($self, $file) = @_;
    local $! = undef;
    
    my $target = File::Spec->catfile('/etc/mock', $file);
    my $link = File::Spec->catfile($self->config_dir, $file);

    if (-l $link or -e $link) {
        unlink $link or croak("Could not remove old `" . $link .
            "': $!\n");
    }
    if (!symlink($target, $link)) {
        croak("Could not inherit mock file `" . $file . "' into `" .
            $self->directory . "': $!\n");
    }
}


# Creates mock configuration and return name of configuration directory and
# base name of configuration file without extension.
# Removal of the directory is on caller discretion.
# Croaks on error.
sub make_configuration {
    my $self = shift;
    $self->config_file;
    $self->inherit_file('logging.ini');
    $self->inherit_file('site-defaults.cfg');

    return ($self->config_dir, $self->config_root);
}

1;
