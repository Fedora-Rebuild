#!/usr/bin/perl
use strict;
use warnings;

use Test::Simple tests => 5;

use Fedora::Rebuild::Solver;
use Fedora::Rebuild::Set::Package;
use Fedora::Rebuild::Package;
use Fedora::Rebuild::RPM;
use threads::shared;

# Prepare packages
# Create and return package with one binary package that always provides its
# unversioned name.
sub build_package {
    my ($name, $provides, $runrequires) = @_;

    my $package = Fedora::Rebuild::Package->new(
        'name' => $name,
        workdir => 't/tmp',
        dist => 'foo',
        target => 'foo',
        message => 'foo',
    );

    $package->provides(shared_clone(
            {
                $name => {
                    $name => [[Fedora::Rebuild::RPM::ANY, '']],
                    %$provides
                }
            }
        ));
    $package->runrequires(shared_clone(
            {
                $name => $runrequires
            }
        ));
    return $package;
}

my $a = build_package(
    'a',
    {},
    { 'a' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

my $b = build_package(
    'b',
    {},
    { 'a' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

my $c = build_package(
    'c',
    {},
    { 'd' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

my $d = build_package(
    'd',
    {},
    { 'c' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

my $e = build_package(
    'e',
    {},
    { 'd' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

# Prepare set of packages
my $packages = Fedora::Rebuild::Set::Package->new();
$packages->insert($a);
$packages->insert($b);
$packages->insert($c);
$packages->insert($d);
$packages->insert($e);

# Prepare solver
my $solver = Fedora::Rebuild::Solver->new(packages => $packages,
    dependencyfilter => sub {0});

# Test
my $message;

# Cycle with 1 edge
ok($solver->is_installable($a, 'a', \$message),
    "a requires itself: $message");
ok($solver->is_installable($b, 'b', \$message),
    "b requires reflexive a: $message");

# Cycle with 2 edges
ok($solver->is_installable($d, 'd', \$message),
    "d requires c that requires d: $message");
ok($solver->is_installable($c, 'c', \$message),
    "c requires d that requires c: $message");
ok($solver->is_installable($e, 'e', \$message),
    "e requiers d that requires c that requires d: $message");
