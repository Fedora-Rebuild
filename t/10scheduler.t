#!/usr/bin/perl
use strict;
use warnings;
use Test::Simple tests => 3;

use Fedora::Rebuild::Scheduler;

sub eqwithundef {
    my ($a, $b) = @_;
    if (defined $a and defined $b and $a eq $b) {
        return 1;
    }
    if (!defined $a and !defined $b) {
        return 1;
    }
    return 0;
}

sub teststatus {
    my ($message, $expected_exception, $expected_retval, $status) = @_;

    ok(
        (
            eqwithundef($expected_exception, $$status[1]) and
            eqwithundef($expected_retval, $$status[0])
        ),
        $message . ', got: exception=' . ($$status[1]//'<undef>') .
            ' retval=' . ($$status[0]//'<undef>')
    );
}

my $scheduler = Fedora::Rebuild::Scheduler->new(1);

sub worker {
    my $arg = shift;
    if ($arg < 0) {
        die("Dying worker as requested\n");
    }
    return $arg;
}

my $job_to_die = $scheduler->schedule(\&worker, -1);
my $job_to_succeed = $scheduler->schedule(\&worker, 1);
my $job_to_fail = $scheduler->schedule(\&worker, 0);

my %finished = $scheduler->finish();

teststatus('First job should die', "Dying worker as requested\n", undef,
    $finished{$job_to_die});
teststatus('Second job should succeed', "", 1,
    $finished{$job_to_succeed});
teststatus('Third job should fail', "", 0,
    $finished{$job_to_fail});
