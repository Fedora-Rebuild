#!/usr/bin/perl
use strict;
use warnings;

use Test::Simple tests => 2;

use Fedora::Rebuild::Solver;
use Fedora::Rebuild::Set::Package;
use Fedora::Rebuild::Package;
use Fedora::Rebuild::RPM;
use threads::shared;
use Storable;
use File::Spec;

# Prepare packages
# Create and return package.
sub build_package {
    my ($name, $provides, $runrequires, $buildrequires) = @_;

    my $package = Fedora::Rebuild::Package->new(
        'name' => $name,
        workdir => 't/tmp',
        dist => 'foo',
        target => 'foo',
        message => 'foo',
    );

    $package->provides(shared_clone($provides));
    $package->runrequires(shared_clone($runrequires));
    $package->requires(shared_clone($buildrequires));
    return $package;
}

# Test
my $message;


# Case 1
{

my $perl_DBI = build_package(
    'perl-DBI',
    {},
    {},
    { 'perl(ExtUtils::MakeMaker)' => [[Fedora::Rebuild::RPM::ANY, '']] },
);

my $perl = build_package(
    'perl',
    {
        '0:perl-ExtUtils-MakeMaker-6.56-162.fc16' => {
            'perl(ExtUtils::MakeMaker)' => [[Fedora::Rebuild::RPM::ANY, '']]
        },
        '4:perl-5.12.3-162.fc16' => {
            'perl(strict)' => [[Fedora::Rebuild::RPM::ANY, '']]
        },
        '0:perl-PathTools-3.31-162.fc16' => {
            'perl(File::Spec::Unix)' => [[Fedora::Rebuild::RPM::ANY, '']],
            'perl(File::Spec)' => [[Fedora::Rebuild::RPM::ANY, '']],
        },
    },
    {
        '0:perl-ExtUtils-MakeMaker-6.56-162.fc16' => {
            'perl(strict)' => [[Fedora::Rebuild::RPM::ANY, '']]
        },
        '4:perl-5.12.3-162.fc16' => {
            'perl(File::Spec::Unix)' => [[Fedora::Rebuild::RPM::ANY, '']]
        },
        '0:perl-PathTools-3.31-162.fc16' => {
            'perl(File::Spec)' => [[Fedora::Rebuild::RPM::ANY, '']]
        },
    },
    {},
);

my $packages = Fedora::Rebuild::Set::Package->new();
$packages->insert($perl);

my $solver = Fedora::Rebuild::Solver->new(packages => $packages);

ok($solver->is_buildable($perl_DBI, \$message),
    "perl-DBI build-requires perl-ExtUtils-MakeMaker " .
    "that is satisfied by perl " .
    "that is satisfied by perl-PathTools " .
    "that requires different symbol provided by perl-PathTool again: $message");
}

{
# Load hashe fo $what for package $name.
sub load_hash {
    my ($name, $what) = @_;
    my $store = 't/regression1';

    my $file = File::Spec->catfile($store, $name . '-' . $what . '.store');
    if (-e $file) {
        return retrieve($file);
    } else {
        return {};
    }
}

# Create and return package. Populate dependencies from files.
sub build_package_from_store {
    my $name = shift;

    return build_package(
        $name,
        load_hash($name, 'provides'),
        load_hash($name, 'runrequires'),
        load_hash($name, 'buildrequires')
    );
}

my $perl = build_package_from_store('perl');
my $perl_Test_Simple = build_package_from_store('perl-Test-Simple');
my $perl_Test_Pod = build_package_from_store('perl-Test-Pod');
my $perl_Crypt_CBC = build_package_from_store('perl-Crypt-CBC');
my $perl_Crypt_DES = build_package_from_store('perl-Crypt-DES');
my $perl_Net_Daemon = build_package_from_store('perl-Net-Daemon');
my $perl_PlRPC = build_package_from_store('perl-PlRPC');
my $perl_DBI = build_package_from_store('perl-DBI');

my $packages = Fedora::Rebuild::Set::Package->new();
$packages->insert($perl);
$packages->insert($perl_Test_Simple);
$packages->insert($perl_Test_Pod);
$packages->insert($perl_Crypt_CBC);
$packages->insert($perl_Crypt_DES);

my $solver = Fedora::Rebuild::Solver->new(
    packages => $packages,
    dependencyfilter => sub {
        $_[0] !~ /^\s*perl\(/ or
            $_[0] =~ /^\s*perl\(:MODULE_COMPAT/;
    }
);

#ok($solver->is_buildable($perl_DBI, \$message),
#    "perl-DBI build-requires perl-ExtUtils-MakeMaker " .
#    "that is satisfied by perl: $message");
ok($solver->is_buildable($perl_Net_Daemon, \$message),
    "perl-Net-Daemon: $message");
}
