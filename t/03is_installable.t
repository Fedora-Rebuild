#!/usr/bin/perl
use strict;
use warnings;

use Test::Simple tests => 2 * 8;

use Fedora::Rebuild::Solver;
use Fedora::Rebuild::Set::Package;
use Fedora::Rebuild::Package;
use Fedora::Rebuild::RPM;
use threads::shared;

# Prepare packages
# Create and return package with one binary package that always provides its
# unversioned name.
sub build_package {
    my ($name, $provides, $runrequires) = @_;

    my $package = Fedora::Rebuild::Package->new(
        'name' => $name,
        workdir => 't/tmp',
        dist => 'foo',
        target => 'foo',
        message => 'foo',
    );

    $package->provides(shared_clone(
            {
                $name => {
                    $name => [[Fedora::Rebuild::RPM::ANY, '']],
                    %$provides
                }
            }
        ));
    $package->runrequires(shared_clone(
            {
                $name => $runrequires
            }
        ));
    return $package;
}

# Do the test. Once with gathering message, once without the message.
sub do_test {
    my ($solver, $method, $package, $binary_package_name, $title,
        $expected_result) = @_;
    my $message;
    ok($expected_result == $solver->$method($package, $binary_package_name, \$message),
        "$title: $message");
    ok($expected_result == $solver->$method($package, $binary_package_name, undef),
        "$title: no message requested");
}


my $a = build_package(
    'a',
    {
        'dual' =>
            [[Fedora::Rebuild::RPM::EQUAL|Fedora::Rebuild::RPM::GREATER, '1']],
    },
    {}
);

my $b1 = build_package(
    'b1',
    {},
    { 'a' => [[Fedora::Rebuild::RPM::ANY, '']] }
);

my $b2 = build_package(
    'b2',
    {},
    {'a' => [[Fedora::Rebuild::RPM::ANY, '']]}
);

my $c = build_package(
    'c',
    {
        'dual' =>
            [[Fedora::Rebuild::RPM::EQUAL|Fedora::Rebuild::RPM::GREATER, '1']],
    },
    { 'nonexistent' => [[Fedora::Rebuild::RPM::ANY, '']] }
);

my $d = build_package(
    'd',
    {},
    {'b1' => [[Fedora::Rebuild::RPM::ANY, '']]}
);

my $e = build_package(
    'e',
    {},
    {
        'b1' => [[Fedora::Rebuild::RPM::ANY, '']],
        'b2' => [[Fedora::Rebuild::RPM::ANY, '']],
    }
);

my $f = build_package(
    'f',
    {},
    {
        'a' => [[Fedora::Rebuild::RPM::ANY, '']],
        'dual' => [[Fedora::Rebuild::RPM::ANY, '']],
    }
);

my $g = build_package(
    'g',
    {},
    {
        'dual' => [
            [Fedora::Rebuild::RPM::GREATER, '1'],
            [Fedora::Rebuild::RPM::LESS, '1'],
            [Fedora::Rebuild::RPM::GREATER, '2'],
        ]
    }
);

my $h = build_package(
    'h',
    {},
    { 'c' => [[Fedora::Rebuild::RPM::ANY, '']] }
);

# Prepare set of packages
my $packages = Fedora::Rebuild::Set::Package->new();
$packages->insert($a);
$packages->insert($b1);
$packages->insert($b2);
$packages->insert($c);
$packages->insert($d);
$packages->insert($e);
$packages->insert($f);
$packages->insert($g);
$packages->insert($h);

# Prepare solver
my $solver = Fedora::Rebuild::Solver->new(packages => $packages,
    dependencyfilter => sub {0});

# Test

# Direct dependencies
do_test($solver, 'is_installable', $a, 'a', 'a does require nothing', 1);
do_test($solver, 'is_installable', $b1, 'b1', 'b1 requires a', 1);
do_test($solver, 'is_installable', $c, 'c', 'c requires nonexistent', 0);

# Indirect dependencies
do_test($solver, 'is_installable', $d, 'd', 'd requires b1 that requires a', 1);
do_test($solver, 'is_installable', $e, 'e',
    'e requires b1 and b2, both require a', 1);
do_test($solver, 'is_installable', $h, 'h',
    'h requires c that is not installable', 0);

# More provides, one is not installable.
# XXX: This is not deterministic as it depend on hash ordering.
do_test($solver, 'is_installable', $f, 'f',
    'f requires a and dual, dual is provided by installable a and ' .
    'uninstallable c', 1);

# More provides, one is not installable, but all requires cannot be satisfied
# at the same time.
do_test($solver, 'is_installable', $g, 'g',
    q{g requires versioned dual's, they cannot be satisfied at the same time},
    0);
